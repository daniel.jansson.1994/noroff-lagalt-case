# LAGALT
This README is for the frontend for the assignment lagalt.
Live functional deployment can be found at https://noroff-lagalt-web-app.herokuapp.com/. 

## Table of contents
1. [Installation](#installation)
2. [Usage](#usage)
2. [Setup](#setup)
3. [Acknowledgment](#acknowledgment)
4. [Contributors](#contributors)

## Installation

To install dependencies, use cmd/bash and cd into the lagalt-frontend folder and run.

```bash
npm install 
```

## Setup
If you want to run the backend locally as well, it will be necessary to setup your own .env file and put in the relevant keys.
REACT_APP_API_URL= url to the API, either local or something you hosted yourself.
REACT_APP_API_KEY= the key to access the API.
REACT_APP_SESSION_KEY = if you setup your own firebase, the session key needs to match your project.
it will look like "firebase:authUser:ID_OF_YOUR_FIREBASE_DATABASE:[DEFAULT]"

If you setup your own firebase database you will need to paste in your own firebase config information in the src/firebase.tsx file.

## Usage

To start the program locally, use cmd/bash and cd into the lagalt-frontend folder and run.


```bash
npm start
```

## Contributors
* [Rosy Oo](https://gitlab.com/rosyoo)
* [Elisabeth Bruce](https://gitlab.com/ethoyer)
* [Daniel Jansson](https://gitlab.com/daniel.jansson.1994)
* [Kristian Jul-Larsen](https://gitlab.com/ludakr1ss)