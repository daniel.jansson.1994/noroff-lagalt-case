module.exports = {
  content: [
    "./public/index.html",
    "./src/**/*.{jsx, js, tsx}",
    "./src/App.tsx"
  ],
  theme: {
    extend: {
      colors: {
        secondary: "var(--lightest-blue)",
        accent: "var(--light-blue)"
      },
    },
  },
  plugins: [],
}
