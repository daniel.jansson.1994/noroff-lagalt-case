import { useEffect, useState } from "react";
import { getAuth } from "firebase/auth";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { storageRead } from "../utils/storage";
import { projectEditingSetTrueAction } from "../store/projectEditingActions";
import { projectApplySetTrueAction } from "../store/projectApplyActions";
import { applicationViewSetTrueAction } from "../store/applicationViewActions";
import { fetchUserApplications } from "../api/applications";

const ProjectDetail = ({ project }) => {
  const [user, setUser] = useState(getAuth().currentUser);
  const [participant, setParticipant] = useState(false);
  const [haveApplied, setHaveApplied] = useState(false);
  const [apiError, setApiError] = useState();

  getAuth().onAuthStateChanged((user) => {
    setUser(user);
  }); // sets user as state when authState has changed(ex. on login)

  // if currently logged in user is either the owner or participating in the project
  // they are currently viewing: sets participant state as true
  // this then hides the "apply to project" button
  const checkIfUserIsParticipant = () => {
    if (storageRead() !== null) {
      if (storageRead().uid === project.OwnerGoogleKey) {
        setParticipant(true);
      }
      for (let i = 0; i < Object.values(project.users).length; i++) {
        if (storageRead().uid === Object.values(project.users)[i]) {
          setParticipant(true);
        }
      }
    }
  };

  const checkIfUserHasApplied = async () => {
    const [error, userResponse] = await fetchUserApplications(
      storageRead().uid
    );
    if (error) {
      setApiError(error);
      console.log(apiError);
    }
    if (userResponse !== null) {
      userResponse.forEach((application) => {
        if (application.projectProjectId === project.id) {
          setHaveApplied(true);
        }
      });
    }
  };

  useEffect(() => {
    let isMounted = true;
    if (isMounted) {
      checkIfUserIsParticipant();
      checkIfUserHasApplied();
    }
    return () => {
      isMounted = false;
    };
  }, []);

  const dispatch = useDispatch();

  const editProject = () => {
    dispatch(projectEditingSetTrueAction());
  };

  const applyToProject = () => {
    dispatch(projectApplySetTrueAction());
  };

  const viewApplications = () => {
    dispatch(applicationViewSetTrueAction());
  };

  return (
    <section className=" p-3 border-2">
      <div className="flex">
        <div className="ml-5 w-10/12">
          <div className="flex">
            <h1 className="text-2xl uppercase text-align center">
              {project.title}
            </h1>
            <a href={`/profil/bruker/${project.OwnerGoogleKey}`}>
              <p className="mt-1 ml-3 text-xl">av {project.owner}</p>
            </a>
          </div>
          <div>
            {user !== null && storageRead().uid === project.OwnerGoogleKey && (
              <button onClick={editProject} className="text-sm underline">
                Rediger prosjekt
              </button>
            )}
          </div>

          <div className="flex mt-4">
            <p className="ml-2 text-lg">{project.projectType}</p>
            <p
              className={`mr-7 ml-4 rounded px-2 py-1 text-sm ${project.projectStatus}`}
            >
              {" "}
              {project.projectStatus}
            </p>
          </div>
          <div className="mt-4">
            <ul
              className="ml-2 flex flex-wrap w-4/5 text-sm italic mt-2"
              id="skillTags"
            >
              {project.skills.map((skill) => {
                return (
                  <li className="mr-3 mb-2 rounded px-2 py-1" key={skill}>
                    {skill}
                  </li>
                );
              })}
            </ul>
          </div>
        </div>
      </div>
      <div className="mt-4 ml-5">
        <div className="mb-2">
          <h2 className="text-md underline ml-2">Om prosjektet</h2>
          <p className="px-2 py-0.5 mt-2 mr-4">{project.description}</p>
        </div>
        {/* shows "apply to project" button/link if user is logged in and not a participant of the project */}
        {user !== null && !participant && !haveApplied && (
          <button className="mt-4 ml-2 button" onClick={applyToProject}>
            delta på prosjekt
          </button>
        )}
        {haveApplied && (
          <>
            <button className="mt-4 ml-2 grey-button" disabled onClick={applyToProject}>
              delta på prosjekt
            </button>
            <p className="text-sm italic">Du har allerede sendt en søknad til dette prosjektet</p>
          </>
        )}
        {/* shows "view all applications" button/link if user owns the project they are viewing */}
        {user !== null && storageRead().uid === project.OwnerGoogleKey && (
          <button className="mt-4 ml-2 button" onClick={viewApplications}>
            Se alle søknader
          </button>
        )}
      </div>
    </section>
  );
};

export default ProjectDetail;
