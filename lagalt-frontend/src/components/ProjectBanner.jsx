import { Link } from "react-router-dom";

const ProjectBanner = ({ project }) => {
  return (
    <Link to={`/prosjekt/${project.id}`}>
      <section className="p-3 border-2 mb-1 active:bg-slate-200 hover:bg-slate-200">
        <div className="ml-4 w-full">
          <div className="flex mb-2">
            <h1 className="text-2xl uppercase mb-2">{project.title}</h1>
            <p className="self-center ml-2 italic text-sm">
              av {project.owner}
            </p>
          </div>
          <div className="flex">
              <p id="projectCategory" className="text-lg">
                {project.projectType}
              </p> 
              <div className="flex w-1/6 mr-2">
              <p id="projectStatus" className={`ml-4 rounded px-2 py-1 text-sm ${project.projectStatus} `}>
                {project.projectStatus}
              </p>
              </div>
            <ul
              id="skillTags"
              className="text-sm w-40 italic mt-2 ml-5 flex flex-wrap"
            >
              {project.skills.map((skill) => {
                return (
                  <li className="mr-3 mb-2 rounded px-2 py-1" key={skill}>
                    {skill}
                  </li>
                );
              })}
            </ul>
          </div>
        </div>
      </section>
    </Link>
  );
};

export default ProjectBanner;
