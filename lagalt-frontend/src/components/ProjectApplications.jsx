import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { fetchApplications } from "../api/applications";
import { applicationViewSetFalseAction } from "../store/applicationViewActions";
import ProjectApplicationItem from "./ProjectApplicationItem";

const ProjectApplications = ({project}) => {
  const [apiError, setApiError] = useState([]);
  const [applicationList, setApplications] = useState([]);

  const dispatch = useDispatch();

  const handleReturnToProject = () => {
    dispatch(applicationViewSetFalseAction());
  }

  const onLoad = async (projectId) => { // gets all projects and saves as state
    const [error, response] = await fetchApplications(projectId);
    if (error) {
      console.log(error);
    }
    if (response !== null) {
      let applicationList = [];
      response.forEach((project) => {
        applicationList.push(project);
      });
      setApplications(applicationList);
    }
  };

  useEffect(() => {
    let isMounted = true;
    if (isMounted) {
      onLoad(project.id);
    }
    return () => {
      isMounted = false;
    };
  }, []);

  return (
    <>
    <h1 className="text-lg mb-3">Prosjekt Søknader til {project.title}</h1>
    {applicationList.map((application) => {
      if(application.deleted === false) {
        return <ProjectApplicationItem application={application} key={application.id} />;
      }
      return null;
      })}
    <button onClick={handleReturnToProject} className="button">Gå tilbake til prosjekt</button>
    </>
  );
};
export default ProjectApplications;
