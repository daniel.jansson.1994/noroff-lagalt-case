import { useState, useEffect } from "react";
import Select from "react-select";
import { useForm } from "react-hook-form";
import { fetchSkills } from "../api/skills";
import { fetchCategories } from "../api/categories";
import { fetchStatusOptions } from "../api/status";
import { collectUser } from "../api/users";
import { storageRead } from "../utils/storage";
import { createProject } from "../api/projects";

const NewProjectForm = () => {
  const [apiError, setApiError] = useState([]);
  const [user, setUser] = useState([]);
  const [skills, setSkills] = useState("");
  const [databaseSkills, setDatabaseSkills] = useState([]);
  const [category, setCategory] = useState("");
  const [databaseCategories, setDatabaseCategories] = useState([]);
  const [status, setStatus] = useState("");
  const [databaseStatus, setDatabaseStatus] = useState([]);
  const [isValid, setIsValid] = useState(false);

  const handleSkillChange = (skill) => {
    setSkills(skill);
    checkIfValid(skill);
  };

  const handleCategoryChange = (category) => {
    setCategory(category);
  };

  const handleStatusChange = (status) => {
    setStatus(status);
  };

  const checkIfValid = (skill) => {
    if (skill.length > 0) {
      setIsValid(true);
    } else {
      setIsValid(false);
    }
  };

  const { register, handleSubmit } = useForm();

  const handleProjectCreation = async (data) => {
    const skillIds = [];
    if (skills.length > 0) {
      skills.forEach((skill) => {
        skillIds.push(skill.value);
      });
    }
    const[error, project] = await createProject(
      data.title,
      status.value,
      category.value,
      skillIds,
      data.description,
      user.id
    );
    if(project !== null){
       window.location =`/prosjekt/${project.id}`
    }
  };

  const getUser = async (userId) => {
    const [error, userResponse] = await collectUser(userId);
    if (error) {
      setApiError(error);
      console.log(apiError);
    }
    if (userResponse !== null) {
      setUser(userResponse);
    }
  };

  const getSkills = async () => {
    const [error, userResponse] = await fetchSkills();
    if (error) {
      setApiError(error);
      console.log(apiError);
    }
    if (userResponse !== null) {
      let skills = [];
      userResponse.forEach((skill) => {
        skills.push({ label: skill.name, value: skill.id });
      });
      setDatabaseSkills(skills);
    }
  };

  const getCategories = async () => {
    const [error, userResponse] = await fetchCategories();
    if (error) {
      setApiError(error);
      console.log(apiError);
    }
    if (userResponse !== null) {
      let categories = [];
      userResponse.forEach((category) => {
        categories.push({
          label: category.projecttype,
          value: category.id,
        });
      });
      setDatabaseCategories(categories);
      setCategory(categories[0]);
    }
  };

  const getStatuses = async () => {
    const [error, userResponse] = await fetchStatusOptions();
    if (error) {
      setApiError(error);
      console.log(apiError);
    }
    if (userResponse !== null) {
      let status = [];
      userResponse.forEach((option) => {
        status.push({
          label: option.status,
          value: option.id,
        });
      });
      setDatabaseStatus(status);
      setStatus(status[0]);
    }
  };

  //calls api/users.jsx to collect skills
  const onLoad = async () => {
    getSkills();
    getCategories();
    getStatuses();
    storageRead();
    const data = storageRead();
    getUser(data.uid);
  };

  useEffect(() => {
    let isMounted = true;
    if (isMounted) {
      onLoad();
    }
    return () => {
      isMounted = false;
    };
  }, []);

  return (
    <div className="container">
      <form onSubmit={handleSubmit(handleProjectCreation)}>
        <fieldset>
          <div className="mt-4">
            <label htmlFor="project_title">Tittel</label>
            <input
              maxLength="50"
              minLength="5"
              name="project_title"
              placeholder="Lagalt"
              {...register("title")}
              className="input"
              required
            />
          </div>
          <div className="mt-4">
            <label htmlFor="project_status">Prosjekt status</label>
                <Select 
                onChange={handleStatusChange} 
                className="w-3/4 border-2" 
                options={databaseStatus}
                value={status}
                />
          </div>
          <div className="mt-4">
            <label htmlFor="project_category">Kategori</label>
            <Select
              options={databaseCategories}
              value={category}
              onChange={handleCategoryChange}
              className="w-3/4 border-2"
            />
          </div>
          <div className="mt-4">
            <label htmlFor="project_skills">Nødvendige ferdigheter</label>
            <Select
              name="skills"
              options={databaseSkills}
              isMulti="true"
              value={skills}
              onChange={handleSkillChange}
              className="w-3/4 border-2"
            />
          </div>
          <div className="mt-4">
            <label htmlFor="project_desc">Prosjektbeskrivelse</label>
            <textarea
              maxLength="1250"
              name="project_desc"
              placeholder="skriv her..."
              {...register("description")}
              className="input"
              required
            />
          </div>
        </fieldset>
        <p className="text-sm text-red-500 mt-1">* alle felt må fylles ut</p>
        <button
          type="submit"
          tabIndex="5"
          className={`mt-4 py-1 px-2 ${
            !isValid
            ? "bg-slate-100 text-slate-500 unvalidbtn"
            : "bg-secondary py-1 px-2 mr-2 hover:bg-accent validbtn"
          }`}
          disabled={!isValid}
        >
          Lag prosjekt
        </button>
      </form>
    </div>
  );
};

export default NewProjectForm;
