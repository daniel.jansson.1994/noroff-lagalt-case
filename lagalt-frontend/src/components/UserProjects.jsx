import { useParams } from "react-router-dom";
import { useDispatch } from "react-redux";
import { useState } from "react";
import { userViewProjectsSetFalseAction } from "../store/userViewProjectsActions";
import { storageRead } from "../utils/storage";
import ProjectBanner from "./ProjectBanner";

const UserProjects = ({projects }) => {

  const dispatch = useDispatch();

  const userIdparam = useParams().googleKey;

  const backToProfile = () => {
    dispatch(userViewProjectsSetFalseAction());
  };

  return (
      <>
      {userIdparam === storageRead().uid && (
         <div className="mt-3">       
         <button onClick={backToProfile} className="underline mb-2">Tilbake til profil</button>
         {projects.map((project) => {
           return <ProjectBanner project={project} key={project.id} />;
         })}
       </div>
     )}
     </>
  );
};

export default UserProjects;
