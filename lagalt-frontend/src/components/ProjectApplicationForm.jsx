import { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { collectUser } from "../api/users";
import { useDispatch } from "react-redux";
import { storageRead } from "../utils/storage";
import { applyToProject } from "../api/users";
import { projectApplySetFalseAction } from "../store/projectApplyActions";
import store from "../store/store";

const ProjectApplicationForm = ({ project }) => {
  const [user, setUser] = useState([]);
  const [apiError, setApiError] = useState([]);
  const [isChecked, setChecked] = useState(false);
  const [isSent, setIsSent] = useState(false);

  const dispatch = useDispatch();

  const getUser = async (userId) => {
    const [error, userResponse] = await collectUser(userId);
    if (error) {
      setApiError(error);
      console.log(apiError);
    }
    if (userResponse !== null) {
      setUser(userResponse);
    }
  };

  const handleApplyToProject = async (data) => {
    const [error, application] = await applyToProject(
      data.motivation,
      project.id, 
      user.id
    )
    if(application !== null){
      setIsSent(true);
      console.log("APPLIED TO PROJECT")
    }
  } 

  const { register, handleSubmit } = useForm();

  const checkboxChange = () => {
      setChecked(!isChecked);
  }

  const onLoad = async () => {
    const data = storageRead();
    getUser(data.uid);
  };

  useEffect(() => {
    let isMounted = true;
    if (isMounted) {
      onLoad();
    }
    return () => {
      isMounted = false;
    };
  }, []);

  const backToProject = () => {
    dispatch(projectApplySetFalseAction())
  }

  return (
    <div className="container">
      {!isSent && (
      <form onSubmit={handleSubmit(handleApplyToProject)}>
        <fieldset>
          <h1 className="text-xl">Søk om å delta på {project.title}</h1>
          <div className="mt-4">
            <label htmlFor="motivation_letter">Hvorfor vil du delta på dette prosjektet?</label>
            <textarea
              maxLength="500"
              name="motivation_letter"
              className="mt-4 px-2 py-1 h-40 border-2 block w-5/6"
              placeholder="Skriv her..."
              {...register("motivation")}
            ></textarea>
          </div>
        </fieldset>
        <div className="mt-4 ml-3 flex w-5/6">
          <label></label>
          <input onChange={checkboxChange} type="checkbox" className="mt-2"></input>
          <p className="inline ml-3">
            ved å sende denne søknaden så erkjenner du at prosjektleder vil se
            din profilinformasjon, inkluderene biografi, tidligere
            prosjekter/porteføjle, og ferdigheter uavhengig av om din profil er skjult eller ikke.
          </p>
        </div>
        <div className="flex justify-center w-5/6 mt-5">
        <button className={`mt-4 py-1 px-2 ${
            !isChecked
            ? "bg-slate-100 text-slate-500 unvalidbtn"
            : "bg-secondary py-1 px-2 mr-2 hover:bg-accent validbtn"
          }`} disabled={!isChecked} type="submit">Send søknad</button>
        </div>
      </form>
      )}
       {isSent && (
         <>
         <p>Søknaden har blitt sendt!</p>
         <button className="button" onClick={backToProject} >Gå tilbake til prosjekt</button>
         </>
       )}
    </div>
  );
};

export default ProjectApplicationForm;
