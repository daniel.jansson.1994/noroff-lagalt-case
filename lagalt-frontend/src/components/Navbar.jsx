import { useState } from "react";
import { Link } from "react-router-dom";
import {
  GoogleAuthProvider,
  getAuth,
  signInWithPopup,
  setPersistence,
  browserSessionPersistence,
  signOut,
} from "firebase/auth";
import app from "../firebase";
import { useDispatch } from "react-redux";
import { userSetAction } from "../store/userActions";
import { loginUser } from "../api/users";
import home from "../img/home.png"; // gives image path

const Navbar = () => {
  const [user, setUser] = useState(getAuth().currentUser);
  const [userGKey, setUserGKey] = useState("");

  const dispatch = useDispatch();

  //checks if user is authenticated through firebase and sets user as state
  getAuth().onAuthStateChanged((data) => {
    const user = data;
    if (user !== null) {
      setUser(user);
      dispatch(userSetAction(user));
      setUserGKey(user.uid);
    }
  });

  const googleProvider = new GoogleAuthProvider();

  const handleGoogleSignIn = () => { //logs user in through google
    const auth = getAuth();
    setPersistence(auth, browserSessionPersistence);
    signInWithPopup(auth, googleProvider)
      .then((result) => {
        // // Google Access Token
        // const credential = GoogleAuthProvider.credentialFromResult(result);
        // const token = credential?.accessToken;

        // runs our own login, which checks if user exists in lagalt's db and creates a new user if they don't
        loginUser(result.user.displayName, result.user.uid);
      })
      .catch((err) => {
        const errorCode = err.code;
        const errorMessage = err.message;
        const email = err.email;
        const credential = GoogleAuthProvider.credentialFromError(err);
        throw new Error(
          errorCode + " " + errorMessage + " " + email + " " + credential
        );
      });
  };

  const handleGoogleSignOut = () => {
    const auth = getAuth();
    signOut(auth)
      .then(() => {
        dispatch(userSetAction(null));
        setUser(null); // sets user state as null after logout'
        window.location = "/";
      })
      .catch((err) => {
        const errorCode = err.code;
        const errorMessage = err.message;
        const email = err.email;
        const credential = GoogleAuthProvider.credentialFromError(err);

        throw new Error(
          errorCode + " " + errorMessage + " " + email + " " + credential
        );
      });
  };
  return (
    <nav className="flex flex-row items-center p-1 justify-between shadow-xs shadow-md">
      <ul className="flex flex-row">
        <li className="mx-2">
          <a href="/">
            <img src={home} alt="hjem " className="w-8" />
          </a>
        </li>
        {user !== null && (
          <li className="mx-2">
            <Link to="/nyttprosjekt">Lag nytt prosjekt</Link>
          </li>
        )}
        {user !== null && (
          <li className="mx-2">
            <a href={`/profil/bruker/${userGKey}`}>Profil</a>
          </li>
        )}
      </ul>
      {user === null && (
        <button className="btn" onClick={handleGoogleSignIn}>
          Logg inn med Google
        </button>
      )}
      {user !== null && (
        <button className="btn" onClick={handleGoogleSignOut}>
          Logg ut
        </button>
      )}
    </nav>
  );
};
export default Navbar;
