import { getAuth } from "firebase/auth";
import { useParams } from "react-router-dom";
import { profileEditingSetTrueAction } from "../store/profileEditingActions";
import { userViewProjectsSetTrueAction } from "../store/userViewProjectsActions";
import { userViewProjectApplicationsSetTrueAction } from "../store/userViewProjectApplicationsActions";
import { useDispatch } from "react-redux";
import { useEffect, useState } from "react";
import { storageRead } from "../utils/storage";
import { connect } from "react-redux";
import ProjectBanner from "./ProjectBanner";
import UserProjects from "./UserProjects";
import store from "../store/store";
import UserProjectApplications from "./UserProjectApplications";
import { fetchProject } from "../api/projects";

const ProfileInformation = ({ user, projects, applications }) => {
  const [userUid, setUserUid] = useState("");
  const [inYourProject, setInYourProject] = useState(false);

  const checkIfUserIsInYourProjects = async() => {
    applications.forEach(async(a) => {
      const [error, response] = await fetchProject(a.projectProjectId);
      if (error) {
        console.log(error);
      }
      if (response !== null) {
        if(response.OwnerGoogleKey === getAuth().currentUser.uid){
          setInYourProject(true);
          return true;
        }
      }
    })
  }

  getAuth().onAuthStateChanged(() => {
    setUserUid(getAuth().currentUser.uid);
  });

  const dispatch = useDispatch();

  const editProfile = () => {
    dispatch(profileEditingSetTrueAction());
  };

  const viewUserProjects = () => {
    dispatch(userViewProjectsSetTrueAction());
  }

  const viewUserProjectApplications = () => {
    dispatch(userViewProjectApplicationsSetTrueAction());
  } 
  const userIdparam = useParams().googleKey;

  useEffect(() => {
    let isMounted = true;
    if (isMounted) {
      checkIfUserIsInYourProjects();
    }
    return () => {
      isMounted = false;
    };
  }, []);

  return (
    <>
    {(store.getState().viewUserProjects && !store.getState().viewUserProjectApplications) && (
    <>
    <UserProjects projects={projects}/>
    </>
    )}

    {(store.getState().viewUserProjectApplications && !store.getState().viewUserProjects) && (
      <>
      <UserProjectApplications applications={applications} />
      </>
    )}

    {(!store.getState().viewUserProjects && !store.getState().viewUserProjectApplications) && (
      <div className="flex flex-wrap justify-between w-4/5">
        {(user.visible || userIdparam === storageRead().uid || inYourProject) && (
          <section className="mb-4 w-2/5">
            {userIdparam === storageRead().uid && (
              <button onClick={editProfile} className="text-sm underline hover:text-blue-700">
                Rediger profil
              </button>
              )}
            <h2 className="mt-2 text-sm text-slate-600">bio</h2>
            <p className="max-w-lg">{user.description}</p>
            <h2 className="text-lg mt-6 underline">{user.name}'s ferdigheter</h2>
            <ul className="flex flex-wrap text-sm italic mt-2" id="userSkillTags">
              {user.skills.map((skill) => {
                return (
                  <li className="mr-3 mb-2 rounded px-2 py-1" key={skill}>
                    {skill}
                  </li>
                );
              })}
            </ul>
          </section>
        )}
        {(userIdparam === storageRead().uid || inYourProject) && (
          <section className="w-3/5">
            <h2 className="text-xl ">Prosjekter</h2>
            <button onClick={viewUserProjects} className="text-sm underline mt-1 hover:text-blue-700">Se alle prosjekter...</button>
            {projects.length !== 0 && (
              <>
                {projects.slice(0,2).map((project) => {
                  return <ProjectBanner project={project} key={project.id} />;
                })}
              </>
            )}
            {userUid === userIdparam && applications.length > 0 && (
              <>
                <h3 className="mt-6 text-xl">Dine søknader</h3>
                <button onClick={viewUserProjectApplications} className="text-sm underline mt-1 hover:text-blue-700"> Se alle dine søknader...</button>
                <>
                  {applications.slice(0,2).map((application) => {
                    return (
                      <section className="border-2 p-4 mb-3" key={application.id}>
                        <a href={`/prosjekt/${application.projectProjectId}`} className="underline mb-2">
                          Søknad til {application.project}
                        </a>
                        <p className="mb-4">{application.motivation}</p>
                      </section>
                    );
                  })}
                </>
              </>
            )}
          </section>
        )}
        {(!inYourProject && (!user.visible && storageRead().uid !== userIdparam)) &&(
          <p className="mt-2 text-gray-500 italic"> Bruker er i privat modus </p>
        )}
      </div>
    )}
 </>
  );
};

function mapStateToProps(state){
  return{
    viewUserProjects: state.viewUserProjects,
    viewUserProjectApplications: state.viewUserProjectApplications
  }
}

export default connect(mapStateToProps)(ProfileInformation);
