import { useState } from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { acceptApplication, denyApplication } from "../api/applications";
import { applicationViewSetTrueAction } from "../store/applicationViewActions";

const ProjectApplicationItem = ({application}) => {
  const [apiError, setApiError] = useState(null);

  const dispatch = useDispatch();

  const handleAcceptApplication = async () => {
    const error = await acceptApplication(application.id);
    if (error) {
      setApiError(error);
      console.log(apiError);
    }
      window.location.reload();
  };

  const handleDenyApplication = async () => {
    const error = await denyApplication(application.id);
    if (error) {
      setApiError(error);
      console.log(apiError);
    }
      window.location.reload();
  };

  return (
    <section className="border p-4 mb-3">
      <p className="underline mb-2">Søknad fra <a href={`/profil/bruker/${application.userGoogleKey}`}>{application.user}</a></p>
      <p className="mb-4">{application.motivation}</p>
      <button className="button mr-2" onClick={handleAcceptApplication}>Aksepter søknad</button>
      <button className="bg-secondary py-1 px-2 bg-slate-300 grey-button hover:bg-slate-400" onClick={handleDenyApplication}>Avslå søknad</button>
    </section>
  );
};
export default ProjectApplicationItem;
