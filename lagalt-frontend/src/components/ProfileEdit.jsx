import { profileEditingSetFalseAction } from "../store/profileEditingActions";
import { useDispatch } from "react-redux";
import { useEffect, useState } from "react";
import { fetchSkills, deleteSkills } from "../api/skills";
import { updateUser } from "../api/users";
import Select from "react-select";
import { useForm } from "react-hook-form";
import { storageRead } from "../utils/storage";

const ProfileEdit = ({ user }) => {
  const [apiError, setApiError] = useState(null);
  const [skillList, setSkillList] = useState([]);
  const [databaseSkills, setDatabaseSkills] = useState([]);
  const [selectedSkills, setSelectedSkills] = useState([]);
  const [checked, setChecked] = useState(user.visible);

  const { register, handleSubmit } = useForm();

  //calls api/users.jsx to collect skills
  const onLoad = async () => {
    const [error, userResponse] = await fetchSkills();
    if (error) {
      setApiError(error);
      console.log(apiError);
    }
    if (userResponse !== null) {
      let skills = [];
      setDatabaseSkills(userResponse);
      userResponse.forEach((skill) => {
        skills.push({ label: skill.name, value: skill.name });
      });
      setSkillList(skills);
    }
  };

  useEffect(() => {
    // isMounted is a solution to prevent "Warning: Can't perform a React state update
    // on an unmounted component." by doing an effect cleanup
    let isMounted = true;
    if (isMounted) {
      onLoad();
      let existingSkills = [];
      user.skills.forEach((skill) => {
        existingSkills.push({ label: skill, value: skill });
      });
      setSelectedSkills(existingSkills);
    }
    return () => {
      isMounted = false;
    };
  }, []);

  const dispatch = useDispatch();

  const handleProfileSave = async (data) => {
    let skillsToDeleteWord = [];
    // finds list of skills to delete
    user.skills.forEach((oldSkill) => {
      let skillAdded = false;
      selectedSkills.forEach((skill) => {
        if (skill.value === oldSkill) {
          skillAdded = true; // if skill exists in list of skills: do nothing
        }
      });
      if (!skillAdded) {
        // if skill has been removed from list of skills: add them to skillsToDelete
        skillsToDeleteWord.push({ value: oldSkill });
      }
    });

    //iterates through list to find id of skills that need to be deleted and added
    const getSkillId = (ListOfSkills, listOfIds) => {
      ListOfSkills.forEach((skill) => {
        databaseSkills.forEach((item) => {
          if (skill.value === item.name) {
            listOfIds.push(item.id);
          }
        });
      });
    };

    let skillsToAdd = [];
    let skillsToDelete = [];
    getSkillId(selectedSkills, skillsToAdd);
    getSkillId(skillsToDeleteWord, skillsToDelete);

    if(data.description !== undefined){ // only sends fetch if data is not undefined
      const [error, userResponse] = await updateUser(
        user.name,
        data.description,
        storageRead().uid,
        skillsToAdd,
        !data.checkbox
      );
      if (error) {
        setApiError(error);
        console.log(apiError);
      }
      if (userResponse !== null) {
        if(skillsToDelete.length > 0){
          await deleteSkills(storageRead().uid, skillsToDelete);
        }
        window.location.reload();
      }
    }
  };

  const handleProfileEditCancel = () => {
    dispatch(profileEditingSetFalseAction());
  };

  const handleChange = (selectedOptions) => {
    setSelectedSkills(selectedOptions);
  };

  const checkboxChange = () => {
    if (checked) {
      setChecked(false);
    } else {
      setChecked(true);
    }
  };

  return (
    <section>
      <h1 className="text-lg">Rediger profil</h1>
      <form onSubmit={handleSubmit(handleProfileSave)}>
        <div className="mt-4">
          <label htmlFor="description">Biografi</label>
          <textarea
            maxLength="255"
            name="description"
            type="text"
            defaultValue={user.description}
            {...register("description")}
            className="input"
          />
        </div>

        <div className="mt-4">
          <label htmlFor="skills">Ferdigheter</label>
          <Select
            tabIndex="2"
            name="skills"
            options={skillList}
            isMulti="true"
            value={selectedSkills}
            onChange={handleChange}
            className="w-3/4 border-2"
          />
        </div>

        <div className="mt-4 ml-1 mb-1 flex">
          <label></label>
          <input
            className="mt-2"
            type="checkbox"
            onChange={checkboxChange}
            defaultChecked={!user.visible}
            {...register("checkbox")}
          ></input>
          <p className="text-md ml-2">Privat modus</p>
        </div>
        <div className="ml-6 mr-2 w-3/4">
          <p className=" text-gray-400">
            Ved å velge privat modus, vil profilen din være usynlig for andre
            brukere. Men hvis du sender inn en søknad for å delta på et
            prosjekt, vil prosjekteier kunne se profilen din.
          </p>
        </div>
        <button onClick={handleProfileEditCancel} className="mt-5 grey-button">
          Avbryt
        </button>
        <button
          onClick={handleProfileSave}
          type="submit"
          className="mt-5 button"
        >
          Lagre endringer
        </button>
      </form>
    </section>
  );
};

export default ProfileEdit;
