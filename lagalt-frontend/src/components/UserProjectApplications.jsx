import { useParams } from "react-router-dom";
import { useDispatch } from "react-redux";
import { useState } from "react";
import { userViewProjectApplicationsSetFalseAction } from "../store/userViewProjectApplicationsActions"
import { storageRead } from "../utils/storage";

const UserProjectApplications = ({applications }) => {

  const dispatch = useDispatch();

  const userIdparam = useParams().googleKey;

  const backToProfile = () => {
    dispatch(userViewProjectApplicationsSetFalseAction());
  };

  return (
      <>
      {userIdparam === storageRead().uid && (
         <div className="mt-3">       
         <button onClick={backToProfile} className="underline mb-2">Tilbake til profil</button>
         {applications.map((application) => {
                    return (
                      <section className="border-2 p-4 mb-3">
                        <a href={`/prosjekt/${application.projectId}`} className="underline mb-2">
                          Søknad til {application.project}
                        </a>
                        <p className="mb-4">{application.motivation}</p>
                      </section>
                    );
                  })}
       </div>
     )}
     </>
  );
};

export default UserProjectApplications;
