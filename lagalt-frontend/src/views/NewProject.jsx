import NewProjectForm from '../components/NewProjectForm';
import withAuth from "../hoc/withAuth";

const NewProject = () =>{
    return (
        <div>
        <h1 className="text-lg">Lag nytt prosjekt</h1>
        <NewProjectForm />
        </div>
    );
};

export default withAuth(NewProject);