import { useEffect, useState } from "react";
import { collectUser } from "../api/users";
import { useDispatch } from "react-redux";
import ProfileEdit from "../components/ProfileEdit";
import ProfileInformation from "../components/ProfileInformation";
import withAuth from "../hoc/withAuth";
import { connect } from "react-redux";
import { useParams } from "react-router-dom";
import { profileEditingSetFalseAction } from "../store/profileEditingActions";
import store from "../store/store";
import { fetchProjectsByUser } from "../api/projects";
import { fetchUserApplications } from "../api/applications";

const Profile = () => {
  const [apiError, setApiError] = useState(null);
  const [loading, setLoading] = useState(true);
  const [user, setUser] = useState(null);
  const [projects, setProjects] = useState(null);
  const [applications, setApplications] = useState(null);

  const dispatch = useDispatch();
  const userId = useParams().googleKey;

  const collectUserInformation = async () => {
    //gets user information for the profile the user is trying to view
    const [error, userResponse] = await collectUser(userId);
    if (error) {
      setApiError(error);
      console.log(apiError);
    }
    if (userResponse !== null) {
      setUser(userResponse);
    }
  };

  const collectUserProjects = async () => {
    // collects a list of projects the user is in
    const [error, userResponse] = await fetchProjectsByUser(userId);
    if (error) {
      setApiError(error);
      console.log(apiError);
    }
    if (userResponse !== null) {
      setProjects(userResponse);
    }
  };

  const collectUserApplications = async () => {
    //collects applications the user has sent
    const [error, userResponse] = await fetchUserApplications(userId);
    if (error) {
      setApiError(error);
      console.log(apiError);
    }
    if (userResponse !== null) {
      setApplications(userResponse);
    }
  };

  //calls api/users.jsx to collect user information
  const onLoad = async () => {
    await collectUserInformation();
    await collectUserProjects();
    await collectUserApplications();
    setLoading(false); // sets loading to false when all information has been collected
  };

  useEffect(() => {
    // isMounted is a solution to prevent "Warning: Can't perform a React state update
    // on an unmounted component." by doing an effect cleanup
    let isMounted = true;
    if (isMounted) {
      onLoad();
      dispatch(profileEditingSetFalseAction());
    }
    return () => {
      isMounted = false;
    };
  }, []);

  return (
    <>
      {loading && <h1>Laster ned data til profil...</h1>}
      {!loading && !store.getState().editingProfile && ( //displays ProfileInformation when user is not editing their profile
        <>
          <h1 className="text-2xl">{user.name}</h1>
          <ProfileInformation user={user} projects={projects} applications={applications} />
        </>
      )}
      {!loading && store.getState().editingProfile && (
        <ProfileEdit user={user} /> //displays profileEdit component when user is editing their profile
      )}
    </>
  );
};

// when the global state editingProfile is changed, this component will rerender
function mapStateToProps(state) {
  return {
    editing: state.editingProfile,
  };
}

export default withAuth(connect(mapStateToProps)(Profile));
