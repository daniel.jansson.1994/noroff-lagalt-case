import ProjectApplicationForm from "../components/ProjectApplicationForm";
import withAuth from "../hoc/withAuth";

const ProjectApplication = () =>{
    return (
        <div>
        <h1>Project Application </h1>
        <ProjectApplicationForm />
        </div>
    );
};

export default withAuth(ProjectApplication);