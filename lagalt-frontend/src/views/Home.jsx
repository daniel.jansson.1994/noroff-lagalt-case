import ProjectBanner from "../components/ProjectBanner";
import { useEffect, useState } from "react";
import {
  fetchBySearchAndCategory,
  fetchProjects,
  fetchSearhedResult,
} from "../api/projects";
import Select from "react-select";
import { fetchCategories, fetchProjectByCategory } from "../api/categories";
import { useForm } from "react-hook-form";
import { storageRead } from "../utils/storage";

const Home = () => {
  const [databaseCategories, setDatabaseCategories] = useState([]);
  const [category, setCategory] = useState("");
  const [apiError, setApiError] = useState([]);
  const [projects, setProjects] = useState([]);
  const [loading, setLoading] = useState(true);
  const { register, handleSubmit } = useForm();

  // collects all categories in the database and set them as state
  const getCategories = async () => {
    const [error, userResponse] = await fetchCategories();
    if (error) {
      setApiError(error);
      console.log(apiError);
    }
    if (userResponse !== null) {
      let categories = [{ label: "Alle kategorier", value: "all" }];
      userResponse.forEach((category) => {
        categories.push({
          label: category.projecttype,
          value: category.id,
        });
      });
      setDatabaseCategories(categories);
      setCategory(categories[0]);
    }
  };

  let ugk = "ø";
  if(storageRead() === null){
    ugk = "ø";
  } else {
    ugk = storageRead().uid;
  }

  const onLoad = async () => { // gets all projects and saves as state
    const [error, response] = await fetchProjects(ugk);
    setLoading(false); // sets loading to false when all information has been collected
    if (error) {
      console.log(error);
    }
    if (response !== null) {
      let projectList = [];
      response.forEach((project) => {
        projectList.push(project);
      });
      setProjects(projectList);
    }
  };

  useEffect(() => {
    let isMounted = true;
    if (isMounted) {
      onLoad();
      getCategories();
    }
    return () => {
      isMounted = false;
    };
  }, []); // empty array ensures useEffect is only ran on initial render

  const handleCategorySearch = async () => {
    if (category.value !== "all") { // gets all projects based on category chosen by the user
      const [error, response] = await fetchProjectByCategory(category.label);
      if (error) {
        console.log(error);
      }
      if (response !== null) {
        setProjects(response);
      }
    } else {
      onLoad();
    }
  };

  const handleStringSearch = async (data) => { //gets all projects based on text input
    const [error, response] = await fetchSearhedResult(data.search);
    if (error) {
      console.log(error);
    }
    if (response !== null) {
      setProjects(response);
    }
  };

  const handleStringAndCategorySearch = async (data) => { // gets all projects based on category and text input
    const [error, response] = await fetchBySearchAndCategory(data.search, category.label);
    if (error) {
      console.log(error);
    }
    if (response !== null) {
      setProjects(response);
    }
  };

  const changeCategory = async (category) => {
    setCategory(category);
  };

  const handleSearch = async (data) => { //determines which fetch to use, based on user input
    if(category.value !== "all" && data.search !== ""){
      handleStringAndCategorySearch(data, category);
    } else if(data.search === ""){
      handleCategorySearch(category);
    } else if(category.value === "all" && data.search !== "") {
      handleStringSearch(data);
    }
  };

  return (
    <>
    {loading && <h1>Laster ned data...</h1>}
    {!loading && (   
      <>
      <div className="text-center mb-8">
        <form id="searchForm" onSubmit={handleSubmit(handleSearch)}>
          <input
          id="search"
            maxLength="50"
            type="text"
            className="border-2 mt-1 p-1 w-2/5 inputField"
            placeholder="søk..."
            {...register("search")}
          />
          <Select
            options={databaseCategories}
            value={category}
            onChange={changeCategory}
            className="w-1/2 inline-block m-1 border-2"
          />
          <button className="button ml-2 px-8 py-1 m-1">Søk</button>
        </form>
      </div>
      {projects.map((project) => {
        return <ProjectBanner project={project} key={project.id} />;
      })}
      </>
      )}

    </>
  );
};

export default Home;
