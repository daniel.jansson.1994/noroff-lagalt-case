import { useEffect, useState } from "react";
import store from "../store/store";
import { connect, useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import { fetchProject } from "../api/projects";
import ProjectDetail from "../components/ProjectDetail";
import { projectEditingSetFalseAction } from "../store/projectEditingActions";
import { projectApplySetFalseAction} from "../store/projectApplyActions";
import ProjectEditForm from "../components/ProjectEditForm";
import ProjectApplicationForm from "../components/ProjectApplicationForm";
import ProjectApplications from "../components/ProjectApplications";
import { applicationViewSetFalseAction } from "../store/applicationViewActions";

const Project = () => {
  const [loading, setLoading] = useState(true);
  const [project, setProject] = useState(null);

  const id = useParams().id; //urlParam = project user is currently trying to view

  // sends the url param to fetchProject
  const onLoadProject = async () => {
    const [error, projectResponse] = await fetchProject(id);
    if (error) {
      console.log(error);
    }
    if (projectResponse !== null) {
      setProject(projectResponse);
      setLoading(false);
    }
  };

  const dispatch = useDispatch();
  useEffect(() => {
    let isMounted = true;
    if (isMounted) {
      onLoadProject(); 
      // sets all states as fault on component load
      dispatch(projectEditingSetFalseAction())
      dispatch(projectApplySetFalseAction())
      dispatch(applicationViewSetFalseAction())
    }
    return () => {
      isMounted = false;
    };
  }, []);

  return (
    <>
      {loading && <h1>Laster ned data til prosjekt...</h1>}
      {!loading && !store.getState().editingProject && !store.getState().applyProject && !store.getState().viewApplications && (
        // displays project information if user is not currently editing, applying to or viewing applications to the project
          <ProjectDetail project={project} />
      )
      }
      {!loading && store.getState().editingProject && (
          <ProjectEditForm project={project} />
      )}
       {!loading && store.getState().applyProject && (
          <ProjectApplicationForm project={project} />
      )}
      {!loading && store.getState().viewApplications && (
        <ProjectApplications project={project}/>
      )}
    </>
  );
};
function mapStateToProps(state) {
  return {
    editing: state.editingProject,
    applying: state.applyProject,
    viewing: state.viewApplications
  };
}
export default connect(mapStateToProps)(Project);
