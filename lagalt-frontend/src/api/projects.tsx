import { createHeaders } from ".";

const apiUrl = process.env.REACT_APP_API_URL;

//get all projects from database
export const fetchProjects = async (googleKey: string) => {
  try {
    const response = await fetch(`${apiUrl}/projects/algo/${googleKey}`);

    if (!response.ok) {
      throw new Error("Kunne ikke hente ut prosjekter fra databasen ");
    }
    const projects = await response.json();
    return [null, projects];
  } catch (error: any) {
    return [error.message, null];
  }
};

//get one project by id from database
export const fetchProject = async (id: number) => {
  try {
    const response = await fetch(`${apiUrl}/projects/${id}`);
    if (!response.ok) {
      throw new Error("Kunne ikke hente ut et prosjekt fra databasen ");
    }
    const project = await response.json();
    return [null, project];
  } catch (error: any) {
    return [error.message, null];
  }
};

//edit projectinformation in database
export const editProject = async (
  projectId: string,
  title: string,
  status: string,
  category: string,
  skills: Array<string>,
  description: string
) => {
  try {
    const response = await fetch(`${apiUrl}/projects/${projectId}`, {
      method: "PUT",
      headers: createHeaders(),
      body: JSON.stringify({
        title: title,
        projectStatus: status,
        projectType: category,
        skills: skills,
        description: description,
      }),
    });
    if (!response.ok) {
      throw new Error("Kunne ikke oppdatere prosjektinformasjon");
    }
    const data = await response.json();
    return [null, data];
  } catch (error: any) {
    return [error.message, []];
  }
};

//update project status 
const updateStatus = async (id: number, status: string) => {
  try {
    const response = await fetch(`${apiUrl}/projects/projectstatus/${id}`, {
      method: "PUT",
      headers: createHeaders(),
      body: JSON.stringify(status),
    });
    if (!response.ok) {
      throw new Error("Kunne ikke oppdatere prosjektstatusen");
    }
  } catch (error: any) {
    return [error.message, []];
  }
};

//creates a new project
export const createProject = async (
  title: string,
  status: string,
  category: string,
  skills: Array<string>,
  description: string,
  userId: number
) => {
  try {
    const response = await fetch(`${apiUrl}/projects`, {
      method: "POST",
      headers: createHeaders(),
      body: JSON.stringify({
        title: title,
        projectStatus: status,
        projectType: category,
        skills: skills,
        description: description,
        owner: userId,
      }),
    });
    if (!response.ok) {
      throw new Error("Kunne ikke lage et prosjekt");
    }
    const data = await response.json();
    await updateStatus(data.id, status); //Calls updatestatus to update the project status
    return [null, data];
  } catch (error: any) {
    return [error.message, []];
  }
};

//fetch search result based on search from database
export const fetchSearhedResult = async (stringsearch: string) => {
  try {
    const response = await fetch(`${apiUrl}/projects/search=${stringsearch}`);
    if (!response.ok) {
      throw new Error("Kunne ikke hente ut søk");
    }
    const data = await response.json();
    return [null, data];
  } catch (error: any) {
    return [error.message, null];
  }
};

//fetch search result based on search and category from database
export const fetchBySearchAndCategory = async (
  stringsearch: string,
  category: string
) => {
  try {
    const response = await fetch(
      `${apiUrl}/projects/search=${stringsearch}/category=${category}`
    );
    if (!response.ok) {
      throw new Error("Kunne ikke hente ut søk basert på string og kategori");
    }
    const data = await response.json();
    return [null, data];
  } catch (error: any) {
    return [error.message, null];
  }
};

//fetch projects based on user's googlekey from database
export const fetchProjectsByUser = async (googleKey: string) => {
  try {
    const response = await fetch(`${apiUrl}/projects/user/${googleKey}`);
    if (!response.ok) {
      throw new Error("Kunne ikke hente ut prosjekt til bruker");
    }
    const data = await response.json();
    return [null, data];
  } catch (error: any) {
    return [error.message, null];
  }
};
