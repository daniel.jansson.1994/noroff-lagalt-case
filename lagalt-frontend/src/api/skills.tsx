import { createHeaders } from ".";

const apiUrl = process.env.REACT_APP_API_URL;

//collects a list of all skills saved in db
export const fetchSkills = async () => {
  try {
    const response = await fetch(`${apiUrl}/skills`);
    if (!response.ok) {
      throw new Error("Kunne ikke hente ut liste med ferdigheter fra databasen ");
    }
    const result = await response.json();
    return [null, result];
  } catch (error) {
    console.error(error);
  }
};

//deletes previous skills of user after editing profile information
export const deleteSkills = async (userId: string, skills: Array<number>) => {
  try {
    const response = await fetch(`${apiUrl}/users/${userId}/skills/delete`, {
      method: "PUT",
      headers: createHeaders(),
      body: JSON.stringify(skills),
    });
    if (!response.ok) {
      throw new Error("Kunne ikke slette ferdigheter");
    }
    const data = await response.json();
    return [null, data];
  } catch (error: any) {
    return [error.message, []];
  }
};
