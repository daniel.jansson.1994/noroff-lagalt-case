const apiUrl = process.env.REACT_APP_API_URL;

//fetch project status list from database
export const fetchStatusOptions = async () => {
    try{
        const response = await fetch(`${apiUrl}/projectStatuses`)
        if(!response.ok){
            throw new Error("Kunne ikke hente ut liste med status fra databasen ")
        }
        const result = await response.json()
        return[null, result]
    }
    catch (error){
        console.error(error)
    }
}