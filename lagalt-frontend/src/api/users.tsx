import { createHeaders } from ".";

const apiUrl = process.env.REACT_APP_API_URL;

// collects a single user based on google id
export const collectUser = async (userId: string) => {
  try {
    const response = await fetch(`${apiUrl}/users/${userId}`);
    if (!response.ok) {
      throw new Error("Kunne ikke hente ut brukerinformasjon");
    }
    const user = await response.json();
    return [null, user];
  } catch (error: any) {
    return [error.message, null];
  }
};

//update userinformation in database
export const updateUser = async (name: string, description: string, userId: string, skills: Array<number>, visible: boolean) => {
  try {
    const response = await fetch(`${apiUrl}/users/${userId}`, {
      method: "PUT",
      headers: createHeaders(),
      body: JSON.stringify({
        name: name,
        description: description,
        projects: [],
        skills: skills,
        visible: visible
      }),
    });
    if (!response.ok) {
      throw new Error("Kunne ikke oppdatere brukerinformasjon");
    }
    const data = await response.json();
    return [null, data];
  } catch (error: any) {
    return [error.message, []];
  }
};

//checks if user exists in db using google id, returns a boolean false/true
const checkForUser = async (name: string, googlekey: string) => {
  try {
    const response = await fetch(`${apiUrl}/users/${googlekey}/exists`);
    if (!response.ok) {
       throw new Error("Kunne ikke sjekke om bruker eksisterer i databasen");
    }
    const data = await response.json();
    return [null, data]
   } catch (error: any) {
     console.log(error)
     return [error.message, []];
   }
};

// creates a new user, with a name and google id collected from a persons google account
const createUser = async (name: string, googleKey: string) => {
  try{
    const response = await fetch(`${apiUrl}/users`, {
      method: "POST",
      headers: createHeaders(),
      body: JSON.stringify({
        name: name,
        description: "",
        accomplishments: "",
        visible: true
        })
    })
    if (!response.ok) {
      throw new Error("Kunne ikke lage bruker");
    }
    const data = await response.json();
    return await addGoogleKey(googleKey, data.id)
  }catch(error: any) {
    return [error.message, []]
  }
}

//add googlekey to user in database
const addGoogleKey = async (googleKey: string, userId: number) => {
  try{
    const response = await fetch(`${apiUrl}/users/${userId}/gk`, {
      method: "PUT",
      headers: createHeaders(),
      body: googleKey
    })
    if (!response.ok) {
      throw new Error("Kunne ikke legge til google key til bruker");
    }
    const data = await response.json();
    return [null, data]
  }catch(error: any) {
    return [error.message, []]
  }
}

// runs upon login using google's handleGoogleSignIn
// will first run checkForUser
// if user does not exist in the lagalt api, it will run createUser
export const loginUser = async (name: string, googlekey: string) => {
  const [ checkError, data ] = await checkForUser(name, googlekey)
  if(checkError !== null ){
      console.log(checkError)
      return [checkError, null]
  }
  if(data){
    return [null, data]
  }
    return await createUser(name, googlekey)
}

//send application to database
export const applyToProject = async (motivation: string, projectId: number, userId: number) => {
  try{
    const response = await fetch(`${apiUrl}/projectApplications`, {
      method: "POST",
      headers: createHeaders(),
      body: JSON.stringify({
        motivation: motivation,
        project: projectId,
        user: userId
        })
    })
    if (!response.ok) {
      throw new Error("Kunne ikke delta på prosjekt");
    }
    const data = await response.json();
    return [null, data]
  }catch(error: any) {
    return [error.message, []]
  }
}
