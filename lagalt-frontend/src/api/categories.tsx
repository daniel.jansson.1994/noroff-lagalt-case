const apiUrl = process.env.REACT_APP_API_URL;

//fetch all categories from database
export const fetchCategories = async () => {
  try {
    const response = await fetch(`${apiUrl}/projectTypes`);
    if (!response.ok) {
      throw new Error("Kunne ikke hente ut liste med kategorier fra databasen ");
    }
    const result = await response.json();
    return [null, result];
  } catch (error) {
    console.error(error);
  }
};

//fetch a project by category from database
export const fetchProjectByCategory = async (category: string) => {
  try {
    const response = await fetch(`${apiUrl}/projects/projectType=${category}`);
    if (!response.ok) {
      throw new Error("Kunne ikke hente ut prosjekt basert på kategori fra databasen");
    }
    const projects = await response.json();
    return [null, projects];
  } catch (error: any) {
    return [error.message, null];
  }
};
