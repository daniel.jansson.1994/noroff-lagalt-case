import { storageRead } from "../utils/storage";
const apiKey: any = process.env.REACT_APP_API_KEY;

export const createHeaders = () => {
  return {
    "Content-Type": "application/json",
    "x-api-key": apiKey,
    //"Authorization": "Bearer " + storageRead().stsTokenManager.accessToken
  };
};
