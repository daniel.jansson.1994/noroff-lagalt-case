import { createHeaders } from ".";

const apiUrl = process.env.REACT_APP_API_URL;

//fetch applications based on project's id from database
export const fetchApplications = async (projectId: number) => {
  try {
    const response = await fetch(`${apiUrl}/projectApplications/${projectId}`);

    if (!response.ok) {
      throw new Error("Kunne ikke hente ut søknader fra databasen");
    }
    const applications = await response.json();
    return [null, applications];
  } catch (error: any) {
    return [error.message, null];
  }
};

//fetch applications based on user's googlekey from database
export const fetchUserApplications = async (googleKey: string) => {
  try {
    const response = await fetch(
      `${apiUrl}/projectApplications/userGoogleKey/${googleKey}`
    );
    if (!response.ok) {
      throw new Error("Kunne ikke hente ut brukers søknader fra databasen");
    }
    const data = await response.json();
    return [null, data];
  } catch (error: any) {
    return [error.message, null];
  }
};

//accepts applications, sets as deleted after
export const acceptApplication = async (id: number, status: string) => {
  try {
    const response = await fetch(`${apiUrl}/projectApplications/accept/${id}`, {
      method: "PUT",
      headers: createHeaders(),
      body: JSON.stringify("")
    });
    if (!response.ok) {
      throw new Error("Kunne ikke akseptere søknaden");
    }
  } catch (error: any) {
    return [error.message];
  }
};

//deny application by deleting it
export const denyApplication = async (id: number) => {
  try {
    const response = await fetch(`${apiUrl}/projectApplications/status/${id}`, {
      method: "PUT",
      headers: createHeaders(),
      body: JSON.stringify("")
    });
    if (!response.ok) {
      throw new Error("Kunne ikke slette søknaden");
    }
  } catch (error: any) {
    return [error.message];
  }
};
