const key = process.env.REACT_APP_SESSION_KEY;

export const storageRead = () => {
  const data = sessionStorage.getItem(key);
  if (data) {
    return JSON.parse(data);
  }
  return null;
};
