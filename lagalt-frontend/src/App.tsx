import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Home from './views/Home';
import NewProject from './views/NewProject';
import Profile from './views/Profile';
import Project from './views/Project';
import ProjectApplication from './views/ProjectApplication';

import Navbar from './components/Navbar';

function App() {
  return (
    <BrowserRouter>
    <div className="App">
    <Navbar></Navbar>
    <main className="py-12 md:px-20 sm:px-14 px-6">
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/nyttprosjekt" element={<NewProject />} />
        <Route path="/profil/bruker/:googleKey" element={<Profile />} />
        <Route path="/prosjekt/:id" element={<Project />} />
        <Route path="/prosjekt-soknad" element={<ProjectApplication />} />
      </Routes>
    </main>
    </div>
    </BrowserRouter>
  );
}

export default App;
