import { ACTION_USER_SET } from "./userActions";

export const userReducer = (state = null, action: any) => {
  switch (action.type) {
    case ACTION_USER_SET:
      return action.payload;

    default:
      return state;
  }
};
