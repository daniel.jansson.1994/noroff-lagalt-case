import { ACTION_USER_VIEW_PROJECTS_SET_FALSE, ACTION_USER_VIEW_PROJECTS_SET_TRUE } from "./userViewProjectsActions";

export const userViewProjectsReducer = (state = false, action: any) => {
  switch(action.type) {
    case ACTION_USER_VIEW_PROJECTS_SET_FALSE:
      return false;
    case ACTION_USER_VIEW_PROJECTS_SET_TRUE:
      return true;
    default: 
    return state;
  }
}