export const ACTION_APPLICATION_VIEW_SET_FALSE = '[applicationView] SET FALSE'
export const ACTION_APPLICATION_VIEW_SET_TRUE = '[applicationView] SET TRUE'

export const applicationViewSetFalseAction = () => ({
  type: ACTION_APPLICATION_VIEW_SET_FALSE
})

export const applicationViewSetTrueAction = () => ({
  type: ACTION_APPLICATION_VIEW_SET_TRUE
})