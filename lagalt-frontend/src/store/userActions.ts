export const ACTION_USER_SET = '[user] SET'

export const userSetAction = (payload: any) => ({
  type: ACTION_USER_SET,
  payload
})