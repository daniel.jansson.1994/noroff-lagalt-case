import { applyMiddleware, combineReducers, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import { userReducer } from "./userReducer";
import { profileEditingReducer } from "./profileEditingReducer";
import { projectEditingReducer } from "./projectEditingReducer"
import { projectApplyReducer } from "./projectApplyReducer";
import { applicationViewReducer } from "./applicationViewReducer";
import { userViewProjectsReducer } from "./userViewProjectsReducer";
import { userViewProjectApplicationsReducer} from "./userViewProjectApplicationsReducer";

const rootReducers = combineReducers({
  user: userReducer,
  //selectedUser: userReducer,
  editingProfile: profileEditingReducer,
  //editingProject: editingReducer
  editingProject: projectEditingReducer,
  applyProject: projectApplyReducer,
  viewApplications: applicationViewReducer,
  viewUserProjects: userViewProjectsReducer,
  viewUserProjectApplications: userViewProjectApplicationsReducer
});

export default createStore(
  rootReducers,
  composeWithDevTools(applyMiddleware())
);
