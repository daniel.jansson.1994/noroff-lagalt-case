export const ACTION_PROJECT_EDITING_SET_FALSE = '[projectEditing] SET FALSE'
export const ACTION_PROJECT_EDITING_SET_TRUE = '[projectEditing] SET TRUE'

export const projectEditingSetFalseAction = () => ({
  type: ACTION_PROJECT_EDITING_SET_FALSE
})

export const projectEditingSetTrueAction = () => ({
  type: ACTION_PROJECT_EDITING_SET_TRUE
})