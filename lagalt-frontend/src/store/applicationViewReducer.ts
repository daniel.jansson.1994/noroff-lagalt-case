import { ACTION_APPLICATION_VIEW_SET_FALSE, ACTION_APPLICATION_VIEW_SET_TRUE } from "./applicationViewActions";

export const applicationViewReducer = (state = false, action: any) => {
  switch(action.type) {
    case ACTION_APPLICATION_VIEW_SET_FALSE:
      return false;
    case ACTION_APPLICATION_VIEW_SET_TRUE:
      return true;
    default: 
    return state;
  }
}
