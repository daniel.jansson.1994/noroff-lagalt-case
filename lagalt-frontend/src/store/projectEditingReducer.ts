import { ACTION_PROJECT_EDITING_SET_FALSE, ACTION_PROJECT_EDITING_SET_TRUE } from "./projectEditingActions";

export const projectEditingReducer = (state = false, action: any) => {
  switch (action.type) {
    case ACTION_PROJECT_EDITING_SET_FALSE:
      return false;
    case ACTION_PROJECT_EDITING_SET_TRUE:
      return true;
    default:
      return state;
  }
};