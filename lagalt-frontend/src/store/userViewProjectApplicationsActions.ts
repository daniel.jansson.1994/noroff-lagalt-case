export const ACTION_USER_VIEW_PROJECT_APPLICATIONS_SET_FALSE = '[userViewProjectApplications] SET FALSE'
export const ACTION_USER_VIEW_PROJECT_APPLICATIONS_SET_TRUE = '[userViewProjectApplications] SET TRUE'

export const userViewProjectApplicationsSetFalseAction = () => ({
  type: ACTION_USER_VIEW_PROJECT_APPLICATIONS_SET_FALSE
})

export const userViewProjectApplicationsSetTrueAction = () => ({
  type: ACTION_USER_VIEW_PROJECT_APPLICATIONS_SET_TRUE
})