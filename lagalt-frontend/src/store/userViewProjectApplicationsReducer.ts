import { ACTION_USER_VIEW_PROJECT_APPLICATIONS_SET_FALSE, ACTION_USER_VIEW_PROJECT_APPLICATIONS_SET_TRUE } from "./userViewProjectApplicationsActions";

export const userViewProjectApplicationsReducer = (state = false, action: any) => {
  switch(action.type) {
    case ACTION_USER_VIEW_PROJECT_APPLICATIONS_SET_FALSE:
      return false;
    case ACTION_USER_VIEW_PROJECT_APPLICATIONS_SET_TRUE:
      return true;
    default: 
    return state;
  }
}