import { ACTION_PROJECT_APPLY_SET_FALSE, ACTION_PROJECT_APPLY_SET_TRUE } from "./projectApplyActions";

export const projectApplyReducer = (state = false, action: any) => {
  switch (action.type) {
    case ACTION_PROJECT_APPLY_SET_FALSE:
      return false;
    case ACTION_PROJECT_APPLY_SET_TRUE:
      return true;
    default:
      return state;
  }
};