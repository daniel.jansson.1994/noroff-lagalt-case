export const ACTION_PROFILE_EDITING_SET_FALSE = '[profileEditing] SET FALSE'
export const ACTION_PROFILE_EDITING_SET_TRUE = '[profileEditing] SET TRUE'

export const profileEditingSetFalseAction = () => ({
  type: ACTION_PROFILE_EDITING_SET_FALSE
})

export const profileEditingSetTrueAction = () => ({
  type: ACTION_PROFILE_EDITING_SET_TRUE
})