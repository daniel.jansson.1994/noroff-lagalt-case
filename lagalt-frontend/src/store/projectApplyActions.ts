export const ACTION_PROJECT_APPLY_SET_FALSE = '[projectApply] SET FALSE'
export const ACTION_PROJECT_APPLY_SET_TRUE = '[projectApply] SET TRUE'

export const projectApplySetFalseAction = () => ({
  type: ACTION_PROJECT_APPLY_SET_FALSE
})

export const projectApplySetTrueAction = () => ({
  type: ACTION_PROJECT_APPLY_SET_TRUE
})