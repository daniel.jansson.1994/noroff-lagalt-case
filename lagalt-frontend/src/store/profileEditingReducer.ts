import { ACTION_PROFILE_EDITING_SET_FALSE, ACTION_PROFILE_EDITING_SET_TRUE } from "./profileEditingActions";

export const profileEditingReducer = (state = false, action: any) => {
  switch (action.type) {
    case ACTION_PROFILE_EDITING_SET_FALSE:
      return false;
    case ACTION_PROFILE_EDITING_SET_TRUE:
      return true;
    default:
      return state;
  }
};
