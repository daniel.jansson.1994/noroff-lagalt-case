export const ACTION_USER_VIEW_PROJECTS_SET_FALSE = '[userViewProjects] SET FALSE'
export const ACTION_USER_VIEW_PROJECTS_SET_TRUE = '[userViewProjects] SET TRUE'

export const userViewProjectsSetFalseAction = () => ({
  type: ACTION_USER_VIEW_PROJECTS_SET_FALSE
})

export const userViewProjectsSetTrueAction = () => ({
  type: ACTION_USER_VIEW_PROJECTS_SET_TRUE
})