import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

// Import the functions you need from the SDKs you need
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCwv3BdaZdy2ARQ-0WZYU2GXtbuoYeZfY4",
  authDomain: "lagalt-63857.firebaseapp.com",
  projectId: "lagalt-63857",
  storageBucket: "lagalt-63857.appspot.com",
  messagingSenderId: "937714376641",
  appId: "1:937714376641:web:1f0642bb9cac67cf88d940"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

//initializeApp(firebaseConfig);
export default app;