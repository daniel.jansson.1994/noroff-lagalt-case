import { Navigate } from "react-router-dom";

const withAuth = (Component) => (props) => {
  // authorsizes users who has a currently going logged in session
  // certain views will be blocked if user does not have an ongoing session
  const user = sessionStorage.getItem("firebase:authUser:AIzaSyCwv3BdaZdy2ARQ-0WZYU2GXtbuoYeZfY4:[DEFAULT]");
  
  if (user !== null) {
    return <Component {...props} />;
  } else {
    return <Navigate to="/" />;
  }
};

export default withAuth;