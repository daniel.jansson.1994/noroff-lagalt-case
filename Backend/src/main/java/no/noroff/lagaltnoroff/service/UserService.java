package no.noroff.lagaltnoroff.service;

import no.noroff.lagaltnoroff.DTO.UserCreationDTO;
import no.noroff.lagaltnoroff.DTO.UserDTO;
import no.noroff.lagaltnoroff.DTO.UserUpdateDTO;
import no.noroff.lagaltnoroff.mapper.UserMapper;
import no.noroff.lagaltnoroff.model.*;
import no.noroff.lagaltnoroff.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserService {
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ProjectRepository projectRepository;
	@Autowired
	private SkillRepository skillRepository;
	@Autowired
	private UserHistoryRepository userHistoryRepository;
	@Autowired
	private MessageRepository messageRepository;
	@Autowired
	private UserMapper userMapper;

	/**
	 * Updates the user with the User in the request body.
	 * Fields that can be updated is Name, Description, Accomplishments and visible.
	 *
	 * @param userUpdateDTO User
	 * @param googleKey     String
	 * @return User
	 */
	public User updateUserByGoogleKey(UserUpdateDTO userUpdateDTO, String googleKey) {
		if (userRepository.existsByGoogleKey(googleKey)) {
			User userInDb = userRepository.findUserByGoogleKey(googleKey);
			userInDb.setName(userUpdateDTO.getName());
			userInDb.setDescription(userUpdateDTO.getDescription());
			addSkillsForUserWithGoogleKey(userInDb.getGoogleKey(), userUpdateDTO.getSkills());

			for (long projectId : userUpdateDTO.getProjects()) {
				addProjectByUserGoogleKey(userInDb.getGoogleKey(), projectId);
			}

			userInDb.setVisible(userUpdateDTO.isVisible());
			userRepository.save(userInDb);
			return userInDb;
		} else
			return null;
	}

	// find all users and returns a list of UserDTO's
	public List<UserDTO> findAll() {
		List<User> users = userRepository.findAll();
		List<UserDTO> userDTOS = new ArrayList<>();
		for (User user : users) {
			UserDTO userDTO = userMapper.userToUserDTO(user);
			userDTOS.add(userDTO);
		}
		return userDTOS;
	}

	// Creates a new user from UserCreationDTO and converts it to a user
	public User save(UserCreationDTO userCreationDTO) {
		return userRepository.save(userMapper.userCreationToUser(userCreationDTO));
	}

	// finds the user by the given google key
	public User getUserByGoogleKey(String googleKey) {
		return userRepository.findUserByGoogleKey(googleKey);
	}

	// Check if the google key exists to a user or not
	public boolean userExistsByGoogleKey(String googleKey) {
		return userRepository.existsByGoogleKey(googleKey);
	}

	/**
	 * Adds google key to the user.
	 * Can only be set if the key does not exist.
	 *
	 * @param id        Long
	 * @param googleKey String
	 * @return User
	 */
	public User addGoogleKeyToUserById(long id, String googleKey) {
		if (userRepository.existsById(id)) {
			User userInDb = userRepository.getById(id);
			if (userInDb.getGoogleKey() == null)
				userInDb.setGoogleKey(googleKey);
			userRepository.save(userInDb);
			return userInDb;
		} else
			return null;
	}

	// Adds project to the user by the give google key and project id
	public User addProjectByUserGoogleKey(String googleKey, long projectId) {
		if (userRepository.existsByGoogleKey(googleKey)) {
			User userInDb = userRepository.findUserByGoogleKey(googleKey);

			// Gets the previous projects the user has
			Set<Project> projects = new HashSet<>(userInDb.getProjects());
			if (projectRepository.existsById(projectId)) {
				Project project = projectRepository.getById(projectId);
				projects.add(project);
			}

			userInDb.setProjects(projects);
			userRepository.save(userInDb);
			return userInDb;
		} else
			return null;
	}

	// Delete a project to the user by the given google key and project id.
	public User removeProjectByUserGoogleKey(String googleKey, long projectId) {
		if (userRepository.existsByGoogleKey(googleKey)) {
			User userInDb = userRepository.findUserByGoogleKey(googleKey);

			Set<Project> projects = new HashSet<>(userInDb.getProjects());
			if (projectRepository.existsById(projectId) && projects.contains(projectRepository.getById(projectId))) {
				Project project = projectRepository.findById(projectId).get();
				projects.remove(project);
			}

			userInDb.setProjects(projects);
			userRepository.save(userInDb);
			return userInDb;
		} else
			return null;
	}

	// Add skills to the user by the given google key and a set of longs(Skill id's)
	public User addSkillsForUserWithGoogleKey(String googleKey, Set<Long> skillIds) {
		if (userRepository.existsByGoogleKey(googleKey)) {
			User userInDb = userRepository.findUserByGoogleKey(googleKey);

			Set<Skill> skills = new HashSet<>(userInDb.getSkills());
			for (Long skillId : skillIds) {
				Skill skillInDb = skillRepository.findById(skillId).get();
				skills.add(skillInDb);
			}
			userInDb.setSkills(skills);
			userRepository.save(userInDb);
			return userInDb;
		} else
			return null;
	}

	// Removes skills for the user by the give google key and a set of longs(Skill id's)
	public User deleteSkillsForUserWithGoogleKey(String googleKey, Set<Long> skillIds) {
		if (userRepository.existsByGoogleKey(googleKey)) {
			User userInDb = userRepository.findUserByGoogleKey(googleKey);

			Set<Skill> skills = new HashSet<>(userInDb.getSkills());
			for (Long skillId : skillIds) {
				if (skillRepository.existsById(skillId) && skills.contains(skillRepository.getById(skillId))) {
					Skill skillInDb = skillRepository.getById(skillId);
					skills.remove(skillInDb);
				}
			}
			userInDb.setSkills(skills);
			userRepository.save(userInDb);
			return userInDb;
		} else
			return null;
	}

	// Add user history to the user by the given google key and the user history id
	public User addUserHistoryToUserWithGoogleKey(String googleKey, long uhId) {
		if (userRepository.existsByGoogleKey(googleKey)) {
			User userInDb = userRepository.findUserByGoogleKey(googleKey);

			Set<UserHistory> userHistories = new HashSet<>(userInDb.getUserHistories());
			if (!userHistoryRepository.existsById(uhId)) {
				UserHistory userHistory = userHistoryRepository.getById(uhId);
				userHistories.add(userHistory);
			}

			userInDb.setUserHistories(userHistories);
			userRepository.save(userInDb);
			return userInDb;
		} else
			return null;
	}

	// creates a new message for the user by the given google key, message text, and project id
	public Message addMessageToUserWithGoogleKey(String googleKey, String messageText, long projectId) {
		if (userRepository.existsByGoogleKey(googleKey)) {
			User userInDb = userRepository.findUserByGoogleKey(googleKey);
			Project project = projectRepository.findById(projectId).get();
			String dateSent = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss"));
			Message message = new Message(messageText, dateSent, userInDb, project);
			Set<Message> messages = new HashSet<>(userInDb.getMessages());
			messages.add(message);
			userInDb.setMessages(messages);
			messageRepository.save(message);
			userRepository.save(userInDb);
			return message;
		} else
			return null;
	}

	// "Deletes" the user by the given google key, setting the isDeleted boolean to true.
	public User changeUserStatusById(String googleKey) {
		if (userRepository.existsByGoogleKey(googleKey)) {
			User user = userRepository.findUserByGoogleKey(googleKey);
			user.setDeleted(true);
			userRepository.save(user);
			return user;
		} else
			return null;
	}
}
