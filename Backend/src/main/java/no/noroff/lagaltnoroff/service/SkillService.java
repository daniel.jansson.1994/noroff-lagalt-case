package no.noroff.lagaltnoroff.service;

import no.noroff.lagaltnoroff.model.Skill;
import no.noroff.lagaltnoroff.repositories.SkillRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SkillService {
	@Autowired
	private SkillRepository skillRepository;

	public Skill save(Skill skill) {
		return skillRepository.save(skill);
	}

	public List<Skill> findAll() {
		return skillRepository.findAll();
	}

	public Skill getById(long id) {
		return skillRepository.getById(id);
	}

	// Updates the skill name by the given skill id
	public boolean updateSkillById(Skill skill, long id) {
		if (skillRepository.existsById(id)) {
			Skill skillInDb = skillRepository.getById(id);
			skillInDb.setName(skill.getName());
			skillRepository.save(skillInDb);
			return true;
		} else return false;
	}

	// Deletes the skill by the given skill id
	public boolean deleteSkillById(long id) {
		if (skillRepository.existsById(id)) {
			skillRepository.deleteById(id);
			return true;
		} else return false;
	}
}
