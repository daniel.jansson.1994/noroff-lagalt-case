package no.noroff.lagaltnoroff.service;

import no.noroff.lagaltnoroff.model.Message;
import no.noroff.lagaltnoroff.repositories.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class MessageService {
	@Autowired
	private MessageRepository messageRepository;

	// Updates a message by id
	public boolean updateMessageById(Message message, long id) {
		if (messageRepository.existsById(id)) {
			Message messageInDb = messageRepository.findById(id).get();
			String date = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss"));
			messageInDb.setMessage(message.getMessage());
			messageInDb.setDateSent(date);
			messageInDb.setUser(message.getUser());
			messageRepository.save(messageInDb);
			return true;
		} else return false;
	}

	// Deletes the message by the given id
	public Message deleteMessageById(long id) {
		if (messageRepository.existsById(id)) {
			Message message = messageRepository.findById(id).get();
			message.setDeleted(true);
			messageRepository.save(message);
			return message;
		} else return null;
	}
}
