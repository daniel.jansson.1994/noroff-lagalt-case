package no.noroff.lagaltnoroff.service;

import no.noroff.lagaltnoroff.DTO.ProjectAllDTO;
import no.noroff.lagaltnoroff.DTO.ProjectCreationDTO;
import no.noroff.lagaltnoroff.DTO.ProjectFetchDTO;
import no.noroff.lagaltnoroff.mapper.ProjectMapper;
import no.noroff.lagaltnoroff.model.Project;
import no.noroff.lagaltnoroff.model.ProjectStatus;
import no.noroff.lagaltnoroff.model.Skill;
import no.noroff.lagaltnoroff.model.User;
import no.noroff.lagaltnoroff.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ProjectService {
	@Autowired
	ProjectMapper projectMapper;
	@Autowired
	private ProjectRepository projectRepository;
	@Autowired
	private SkillRepository skillRepository;
	@Autowired
	private ProjectStatusRepository projectStatusRepository;
	@Autowired
	private ProjectTypeRepository projectTypeRepository;
	@Autowired
	private UserRepository userRepository;

	// Gets a project by the given id
	public Project getById(long id) {
		if (projectRepository.existsById(id)) {
			Project projectInDb = projectRepository.findById(id).get();
			projectInDb.setProjectStatus(projectInDb.getProjectStatus());
			projectInDb.setProjectType(projectInDb.getProjectType());
			return projectInDb;
		} else return null;
	}

	// "Deletes" a project by the given id. Sets the isDeleted boolean to true.
	public Project deleteProjectById(long id) {
		if (projectRepository.existsById(id)) {
			Project project = projectRepository.findById(id).get();
			project.setDeleted(true);
			projectRepository.save(project);
			return project;
		} else
			return null;
	}

	// Gets all projects by the given project type string
	public List<Project> getProjectsByCategory(String projecttype) {
		return projectRepository.findAll().stream()
				.filter(p -> p.getProjectType().getProjecttype().equals(projecttype))
				.collect(Collectors.toList());
	}

	// Gets all projects by the given category string
	public List<Project> getProjectsWithNameByCategory(String stringsearch, String projecttype) {
		return projectRepository.findAll().stream()
				.filter(p -> p.getTitle().toLowerCase(Locale.ROOT)
						.contains(stringsearch.toLowerCase(Locale.ROOT))
						&& p.getProjectType().getProjecttype().equals(projecttype))
				.collect(Collectors.toList());
	}

	// Gets the projects matching the string
	public List<Project> getProjectsWithNameLike(String stringsearch) {
		return projectRepository.findAll().stream()
				.filter(p -> p.getTitle().toLowerCase(Locale.ROOT)
						.contains(stringsearch.toLowerCase(Locale.ROOT)))
				.collect(Collectors.toList());
	}

	// Changes the title for the project by the given id
	public Project newTitleForProjectWithId(long id, String projectTitle) {
		if (projectRepository.existsById(id)) {
			Project project = projectRepository.getById(id);
			Project projectInDb = projectRepository.getById(project.getId());
			projectInDb.setTitle(projectTitle);
			projectRepository.save(projectInDb);
			return projectInDb;
		} else
			return null;
	}

	// Changes the description for the project with the given id
	public Project newDescriptionForProjectWithId(long id, String projectDescription) {
		if (projectRepository.existsById(id)) {
			Project project = projectRepository.getById(id);
			Project projectInDb = projectRepository.getById(project.getId());
			projectInDb.setDescription(projectDescription);
			projectRepository.save(projectInDb);
			return projectInDb;
		} else
			return null;
	}


	public Project addSkillsForProjectWithId(long id, Set<Long> skillIds) {
		if (projectRepository.existsById(id)) {
			Project projectInDb = projectRepository.getById(id);

			Set<Skill> skills = new HashSet<>(projectInDb.getSkills());
			for (Long skillId : skillIds) {
				Skill skillInDb = skillRepository.getById(skillId);
				skills.add(skillInDb);
			}
			projectInDb.setSkills(skills);
			projectRepository.save(projectInDb);
			return projectInDb;
		} else
			return null;
	}

	// Changes the project status by the given project id
	public Project setNewProjectStatusWithId(long id, long statusId) {
		if (projectRepository.existsById(id)) {
			Project project = projectRepository.getById(id);
			Project projectInDb = projectRepository.getById(project.getId());
			ProjectStatus ps = projectStatusRepository.getById(statusId);

			projectInDb.setProjectStatus(ps);
			projectRepository.save(projectInDb);
			return projectInDb;
		} else
			return null;
	}

	// Creates new project from ProjectCreationDTO.
	public Project newProjectCreator(ProjectCreationDTO projectCreationDTO) {
		Project project = new Project();
		project.setTitle(projectCreationDTO.getTitle());
		project.setProjectType(projectTypeRepository.findById(projectCreationDTO.getProjectType()).get());
		project.setProjectStatus(projectStatusRepository.findById(1L).get());
		project.setOwner(userRepository.findById(projectCreationDTO.getOwner()).get());
		project.setDescription(projectCreationDTO.getDescription());
		Set<Skill> skillList = new HashSet<>();
		for (long skillId : projectCreationDTO.getSkills()) {
			Skill skill = skillRepository.findById(skillId).get();
			skillList.add(skill);
		}
		project.setSkills(skillList);
		return projectRepository.save(project);
	}

	// Updates the project by the given id
	public Project changeProjectById(ProjectFetchDTO projectFetchDTO, long id) {
		if (projectRepository.existsById(id)) {
			Project project = projectRepository.findById(id).get();
			project.setProjectType(projectTypeRepository.findById(projectFetchDTO.getProjectType()).get());
			project.setTitle(projectFetchDTO.getTitle());
			project.setProjectStatus(projectStatusRepository.findById(projectFetchDTO.getProjectStatus()).get());
			project.setDescription(projectFetchDTO.getDescription());
			Set<Skill> skillList = new HashSet<>();
			for (long skillId : projectFetchDTO.getSkills()) {
				Skill skill = skillRepository.findById(skillId).get();
				skillList.add(skill);
			}
			project.setSkills(skillList);
			return projectRepository.save(project);
		} else
			return null;
	}

	// Get projects a user has by its google key
	public List<Project> getByUserGoogleKey(String key) {
		List<Project> projects = new ArrayList<>();

		projects.addAll(projectRepository.findProjectsByOwnerGoogleKey(key));
		projects.addAll(projectRepository.findProjectsByUsersGoogleKey(key));

		return projects;
	}

	// Algorithm to sort projects by the most matching skills
	public List<ProjectAllDTO> getAllProjectsSortedBySkills(String googleKey) {
		if (googleKey == null || googleKey.isEmpty() || !userRepository.existsByGoogleKey(googleKey)) {
			return projectMapper.projectToProjectAllDto(projectRepository.findAll());
		}

		if (userRepository.existsByGoogleKey(googleKey)) {
			User user = userRepository.findUserByGoogleKey(googleKey);
			Set<Skill> userSkills = user.getSkills();

			List<Project> projectsInDb = projectRepository.findAll();

			HashMap<Long, Integer> skillCounter = new HashMap<>();
			for (Project projectInDb : projectsInDb) {
				int counter = 0;
				Set<Skill> projectSkills = projectInDb.getSkills();
				for (Skill projectSkill : projectSkills) {
					if (userSkills.contains(projectSkill)) {
						counter++;
					}
				}
				skillCounter.put(projectInDb.getId(), counter);
			}
			HashMap<Long, Integer> sortedHashMap = sortByValue(skillCounter);

			List<Project> returnProjects = new ArrayList<>();

			for (Long projectId : sortedHashMap.keySet()) {
				returnProjects.add(projectRepository.getById(projectId));
			}
			return projectMapper.projectToProjectAllDto(returnProjects);
		} else {
			return projectMapper.projectToProjectAllDto(projectRepository.findAll());

		}
	}

	// function to sort hashmap by values
	public static HashMap<Long, Integer> sortByValue(HashMap<Long, Integer> hm) {
		return hm.entrySet()
				.stream()
				.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
				.collect(Collectors.toMap(
						Map.Entry::getKey,
						Map.Entry::getValue,
						(e1, e2) -> e1, LinkedHashMap::new));
	}
}
