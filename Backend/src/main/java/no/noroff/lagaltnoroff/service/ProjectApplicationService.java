package no.noroff.lagaltnoroff.service;

import no.noroff.lagaltnoroff.DTO.ProjectApplicationDTO;
import no.noroff.lagaltnoroff.model.Project;
import no.noroff.lagaltnoroff.model.ProjectApplication;
import no.noroff.lagaltnoroff.model.User;
import no.noroff.lagaltnoroff.repositories.ProjectApplicationRepository;
import no.noroff.lagaltnoroff.repositories.ProjectRepository;
import no.noroff.lagaltnoroff.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class ProjectApplicationService {
	@Autowired
	private ProjectApplicationRepository projectApplicationRepository;
	@Autowired
	private ProjectRepository projectRepository;
	@Autowired
	private UserRepository userRepository;

	// "Deletes" the project application by the given id. Sets the isDeleted boolean to true
	public ProjectApplication deleteProjectApplicationById(long id) {
		if (projectApplicationRepository.existsById(id)) {
			ProjectApplication pa = projectApplicationRepository.findById(id).get();
			pa.setDeleted(true);
			projectApplicationRepository.save(pa);
			return pa;
		} else
			return null;
	}

	// Adding a new project application
	public ProjectApplication addNewApplication(ProjectApplicationDTO projectApplicationDTO) {
		ProjectApplication projectApplication = new ProjectApplication();
		for (ProjectApplication pa : projectApplicationRepository.findAll()) {
			// checks if an application already exists with the user id and project id so a user cannot spam a project with applications
			if (pa.getProject().getId() == projectApplicationDTO.getProject() && pa.getUser().getId() == projectApplicationDTO.getUser()) {
				if (pa.isDeleted()){
				pa.setDeleted(false);
				pa.setMotivation(projectApplicationDTO.getMotivation());
				projectApplicationRepository.save(pa);
				return pa;
				} else
					return null;
			}
		}
		projectApplication.setProject(projectRepository.findById(projectApplicationDTO.getProject()).get());
		projectApplication.setDeleted(false);
		projectApplication.setUser(userRepository.findById(projectApplicationDTO.getUser()).get());
		projectApplication.setMotivation(projectApplicationDTO.getMotivation());
		projectApplicationRepository.save(projectApplication);
		return projectApplication;
	}

	//Find all project applications
	public List<ProjectApplication> findAllApplicationsByProjectId(long id) {
		List<ProjectApplication> projectApplications = projectApplicationRepository.findProjectApplicationsByProjectId(id);
		for (ProjectApplication pa : projectApplicationRepository.findProjectApplicationsByProjectId(id)) {
			if (pa.isDeleted()) {
				projectApplications.remove(pa);
			}
		}
		return projectApplications;
	}

	// Accepts the project application and sets the
	public ProjectApplication acceptProjectApplicationById(long id) {
		ProjectApplication projectApplication = projectApplicationRepository.getById(id);
		Project project = projectApplication.getProject();
		User user = projectApplication.getUser();

		Set<Project> projects = user.getProjects();
		projects.add(project);

		user.setProjects(projects);
		userRepository.save(user);
		deleteProjectApplicationById(id);
		return projectApplication;
	}

	// Finds all applications the user has by its google key
	public List<ProjectApplication> findAllApplicationsByUserGoogleKey(String key) {
		List<ProjectApplication> projectApplications = projectApplicationRepository.findProjectApplicationsByUserGoogleKey(key);
		projectApplications.removeIf(ProjectApplication::isDeleted);
		return projectApplications;
	}
}
