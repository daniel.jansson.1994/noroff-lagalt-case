package no.noroff.lagaltnoroff.service;

import no.noroff.lagaltnoroff.model.UserHistory;
import no.noroff.lagaltnoroff.repositories.UserHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * We do not use user histories. We kept this for future development.
 * This was not on our MVP. We also use skills to show recommended projects.
 */

@Service
public class UserHistoryService {

	@Autowired
	private UserHistoryRepository userHistoryRepository;

	// creates a new user history
	public UserHistory save(UserHistory userHistory) {
		return userHistoryRepository.save(userHistory);
	}

	// Finds all user histories
	public List<UserHistory> findAll() {
		return userHistoryRepository.findAll();
	}

	// Gets a user history by the given id
	public UserHistory getById(long id) {
		return userHistoryRepository.findById(id).get();
	}

	// Updates the user history by the given id
	public boolean updateUserHistoryById(UserHistory userHistory, long id) {
		if (userHistoryRepository.existsById(id)) {
			UserHistory userHistoryInDb = userHistoryRepository.findById(id).get();
			userHistoryInDb.setName(userHistory.getName());
			userHistoryRepository.save(userHistoryInDb);
			return true;
		} else return false;
	}

	// Deletes the user history by its id
	public UserHistory deleteUserHistoryById(long id) {
		if (userHistoryRepository.existsById(id)) {
			UserHistory userHistory = userHistoryRepository.findById(id).get();
			userHistory.setDeleted(true);
			userHistoryRepository.save(userHistory);
			return userHistory;
		} else return null;
	}
}
