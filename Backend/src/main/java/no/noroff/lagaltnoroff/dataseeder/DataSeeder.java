package no.noroff.lagaltnoroff.dataseeder;

import no.noroff.lagaltnoroff.model.ProjectStatus;
import no.noroff.lagaltnoroff.model.ProjectType;
import no.noroff.lagaltnoroff.model.Skill;
import no.noroff.lagaltnoroff.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class DataSeeder implements ApplicationRunner {
	@Autowired
	MessageRepository messageRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	ProjectTypeRepository projectTypeRepository;
	@Autowired
	SkillRepository skillRepository;
	@Autowired
	UserHistoryRepository userHistoryRepository;
	@Autowired
	UserRepository userRepository;
	@Autowired
	ProjectStatusRepository projectStatusRepository;
	@Autowired
	ProjectApplicationRepository projectApplicationRepository;


	@Override
	public void run(ApplicationArguments args) throws Exception {
		loadData();
	}

	private void loadData() {
		String date = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss"));

		if (messageRepository.count() == 0 && projectRepository.count() == 0 && projectTypeRepository.count() == 0 && skillRepository.count() == 0 && userHistoryRepository.count() == 0 && userRepository.count() == 0) {
			final ProjectType pt1 = projectTypeRepository.save(new ProjectType("Web Utvikling"));
			final ProjectType pt2 = projectTypeRepository.save(new ProjectType("Spill Utvikling"));
			final ProjectType pt3 = projectTypeRepository.save(new ProjectType("Musikk"));
			final ProjectType pt4 = projectTypeRepository.save(new ProjectType("Film"));
			final ProjectStatus ps1 = projectStatusRepository.save(new ProjectStatus("Startfasen"));
			final ProjectStatus ps2 = projectStatusRepository.save(new ProjectStatus("Planlegging"));
			final ProjectStatus ps3 = projectStatusRepository.save(new ProjectStatus("Arbeid pågår"));
			final ProjectStatus ps4 = projectStatusRepository.save(new ProjectStatus("Sluttfasen"));
			final ProjectStatus ps5 = projectStatusRepository.save(new ProjectStatus("Siste innspurt"));
			final ProjectStatus ps6 = projectStatusRepository.save(new ProjectStatus("Fullført"));
			final Skill s1 = skillRepository.save(new Skill("HTML"));
			final Skill s2 = skillRepository.save(new Skill("CSS"));
			final Skill s3 = skillRepository.save(new Skill("JavaScript"));
			final Skill s4 = skillRepository.save(new Skill("Java"));
			final Skill s5 = skillRepository.save(new Skill("Photoshop"));
			final Skill s6 = skillRepository.save(new Skill("Tegning"));
			final Skill s7 = skillRepository.save(new Skill("Kunst"));
			final Skill s8 = skillRepository.save(new Skill("Skuespiller"));
			final Skill s9 = skillRepository.save(new Skill("Gitar"));
			final Skill s10 = skillRepository.save(new Skill("Bass"));
			final Skill s11 = skillRepository.save(new Skill("Trommer"));
			final Skill s12 = skillRepository.save(new Skill("Piano"));
			final Skill s13 = skillRepository.save(new Skill("Pixel art"));
			final Skill s14 = skillRepository.save(new Skill("Animasjon"));
			final Skill s15 = skillRepository.save(new Skill("Spill fysikk"));
			final Skill s16 = skillRepository.save(new Skill("Skuespillerinne"));
			final Skill s17 = skillRepository.save(new Skill("Direktør"));
			final Skill s18 = skillRepository.save(new Skill("Sanger"));
			final Skill s19 = skillRepository.save(new Skill("Spill logikk"));
			final Skill s20 = skillRepository.save(new Skill("Spill design"));
			final Skill s21 = skillRepository.save(new Skill("Python"));
			final Skill s22 = skillRepository.save(new Skill("Git"));
			final Skill s23 = skillRepository.save(new Skill("Scrum"));
			final Skill s24 = skillRepository.save(new Skill("Kanban"));
			final Skill s25 = skillRepository.save(new Skill("DevOps"));
			final Skill s26 = skillRepository.save(new Skill("C#"));
			final Skill s27 = skillRepository.save(new Skill("Kotlin"));
			final Skill s28 = skillRepository.save(new Skill("C++"));
			final Skill s29 = skillRepository.save(new Skill("Database"));
			final Skill s30 = skillRepository.save(new Skill("Android Utvikling"));
			final Skill s31 = skillRepository.save(new Skill("IOS Utvikling"));
			final Skill s32 = skillRepository.save(new Skill("Azure"));
			final Skill s33 = skillRepository.save(new Skill("PhP"));
			final Skill s34 = skillRepository.save(new Skill("Komponist"));
			final Skill s35 = skillRepository.save(new Skill("DJ"));
			final Skill s36 = skillRepository.save(new Skill("Stuntman"));
			final Skill s37 = skillRepository.save(new Skill("Stuntkvinne"));
			final Skill s38 = skillRepository.save(new Skill("Klassisk"));
			final Skill s39 = skillRepository.save(new Skill("Country"));
			final Skill s40 = skillRepository.save(new Skill("R&B"));
			final Skill s41 = skillRepository.save(new Skill("Hardstyle"));
			final Skill s42 = skillRepository.save(new Skill("Folkemusikk"));
			final Skill s43 = skillRepository.save(new Skill("Rock"));
			final Skill s44 = skillRepository.save(new Skill("Metal"));
			final Skill s45 = skillRepository.save(new Skill("Triangel"));
			final Skill s46 = skillRepository.save(new Skill("Pyrotekniker"));
			final Skill s47 = skillRepository.save(new Skill("Lystekniker"));
			final Skill s48 = skillRepository.save(new Skill("Lydtekniker"));
			final Skill s49 = skillRepository.save(new Skill("Scenetekniker"));
			final Skill s50 = skillRepository.save(new Skill("Manusforfatter"));
			final Skill s51 = skillRepository.save(new Skill("Personlig assistent"));
			final Skill s52 = skillRepository.save(new Skill("Kameraoperatør"));
			final Skill s53 = skillRepository.save(new Skill("Rekvisittspesialist"));
			final Skill s54 = skillRepository.save(new Skill("Klipper"));
			final Skill s55 = skillRepository.save(new Skill("Kostyme designer"));
			final Skill s56 = skillRepository.save(new Skill("Sminkør"));
			final Skill s57 = skillRepository.save(new Skill("VFX-artist"));
			final Skill s58 = skillRepository.save(new Skill("Utstyrsansvarlig"));
			final Skill s59 = skillRepository.save(new Skill("Logistikkansvarlig"));
			final Skill s60 = skillRepository.save(new Skill("Produsent"));
			final Skill s61 = skillRepository.save(new Skill("Frisør"));
			final Skill s62 = skillRepository.save(new Skill("Unity"));
			final Skill s63 = skillRepository.save(new Skill("Unreal Engine"));
			final Skill s64 = skillRepository.save(new Skill("REST-API"));
			final Skill s65 = skillRepository.save(new Skill("UI designer"));
			final Skill s66 = skillRepository.save(new Skill("Problemløser"));
			final Skill s67 = skillRepository.save(new Skill("VR"));
			final Skill s68 = skillRepository.save(new Skill("Maya"));
			final Skill s69 = skillRepository.save(new Skill("Adobe Photoshop"));
		}
	}
}
