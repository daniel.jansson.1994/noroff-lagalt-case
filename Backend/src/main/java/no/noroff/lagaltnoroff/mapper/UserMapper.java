package no.noroff.lagaltnoroff.mapper;

import no.noroff.lagaltnoroff.DTO.UserCreationDTO;
import no.noroff.lagaltnoroff.DTO.UserDTO;
import no.noroff.lagaltnoroff.DTO.UserSmallDTO;
import no.noroff.lagaltnoroff.model.User;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
public interface UserMapper {
	// Convert user to DTO's using Mapstruct

	UserDTO userToUserDTO(User user);

	User userCreationToUser(UserCreationDTO userCreationDTO);

	UserSmallDTO userToUserSmallDTO(User user);
}
