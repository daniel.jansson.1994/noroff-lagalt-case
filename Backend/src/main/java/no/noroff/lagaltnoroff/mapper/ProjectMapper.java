package no.noroff.lagaltnoroff.mapper;

import no.noroff.lagaltnoroff.DTO.ProjectAllDTO;
import no.noroff.lagaltnoroff.DTO.ProjectDTO;
import no.noroff.lagaltnoroff.model.Project;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(componentModel = "spring")
public interface ProjectMapper {
	// Converts project to DTO's using Mapstruct

	ProjectDTO projectToProjectDto(Project project);

	List<ProjectAllDTO> projectToProjectAllDto(List<Project> project);
}
