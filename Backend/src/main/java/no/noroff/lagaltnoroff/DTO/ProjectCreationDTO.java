package no.noroff.lagaltnoroff.DTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Set;

@Data
@Builder
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ProjectCreationDTO implements Serializable {
	private String title;
	private Long projectType;
	private String description;
	private Set<Long> skills;
	private Long owner;
}
