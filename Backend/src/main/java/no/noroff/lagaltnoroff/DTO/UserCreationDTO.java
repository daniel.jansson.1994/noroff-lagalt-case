package no.noroff.lagaltnoroff.DTO;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserCreationDTO {
	private String name;
	private String description;
	private String accomplishments;
	private boolean isVisible;
}
