package no.noroff.lagaltnoroff.DTO;

import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.Builder;
import lombok.Data;
import no.noroff.lagaltnoroff.model.ProjectStatus;
import no.noroff.lagaltnoroff.model.ProjectType;
import no.noroff.lagaltnoroff.model.Skill;
import no.noroff.lagaltnoroff.model.User;

import java.util.Set;
import java.util.stream.Collectors;

@Data
@Builder
public class ProjectAllDTO {
	private long id;
	private String title;
	private ProjectType projectType;
	private ProjectStatus projectStatus;
	private Set<Skill> skills;
	private User owner;

	@JsonGetter("projectType")
	public String getProjectTypeAsString() {
		return projectType.getProjecttype();
	}

	@JsonGetter("projectStatus")
	public String getProjectStatusAsString() {
		return projectStatus.getStatus();
	}

	@JsonGetter("skills")
	public Set<String> getSkillsAsString() {
		if (skills != null)
			return skills.stream().map(Skill::getName).collect(Collectors.toSet());
		else
			return null;
	}

	@JsonGetter("owner")
	public String getOwnerAsString() {
		if (owner != null)
			return owner.getName();
		else
			return null;
	}

	@JsonGetter("ownerGoogleKey")
	public String getOwnerGoogleKeyAsString() {
		if (owner != null)
			return owner.getGoogleKey();
		else
			return null;
	}
}
