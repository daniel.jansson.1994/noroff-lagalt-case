package no.noroff.lagaltnoroff.DTO;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SkillCreationDTO {
	private String name;
}
