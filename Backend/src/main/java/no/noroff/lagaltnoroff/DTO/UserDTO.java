package no.noroff.lagaltnoroff.DTO;

import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.Builder;
import lombok.Data;
import no.noroff.lagaltnoroff.model.Project;
import no.noroff.lagaltnoroff.model.ProjectApplication;
import no.noroff.lagaltnoroff.model.Skill;

import java.util.Set;
import java.util.stream.Collectors;

@Data
@Builder
public class UserDTO {
	private long id;
	private String name;
	private String description;
	private Set<ProjectApplication> projectApplications;
	private Set<Project> projects;
	private Set<Skill> skills;
	private String googleKey;
	private boolean visible;

	@JsonGetter("projects")
	public Set<String> getProjectsAsString() {
		if (projects != null)
			return projects.stream().map(Project::getTitle).collect(Collectors.toSet());
		else
			return null;
	}

	@JsonGetter("skills")
	public Set<String> getSkillsAsString() {
		if (skills != null)
			return skills.stream().map(Skill::getName).collect(Collectors.toSet());
		else
			return null;
	}

	@JsonGetter("projectApplications")
	public Set<String> getProjectApplicationsAsString() {
		if (projectApplications != null)
			return projectApplications.stream().map(ProjectApplication::getProjectAsString).collect(Collectors.toSet());
		else
			return null;
	}
}
