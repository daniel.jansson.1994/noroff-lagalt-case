package no.noroff.lagaltnoroff.DTO;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserSmallDTO {
	private String name;
	private String googleKey;
}
