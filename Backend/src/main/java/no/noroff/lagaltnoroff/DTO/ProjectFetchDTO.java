package no.noroff.lagaltnoroff.DTO;

import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Data
@Builder
public class ProjectFetchDTO {
	private String title;
	private Long projectType;
	private Long projectStatus;
	private Set<Long> skills;
	private String description;
}
