package no.noroff.lagaltnoroff.DTO;

import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Data
@Builder
public class UserUpdateDTO {
	private String name;
	private String description;
	private Set<Long> projects;
	private Set<Long> skills;
	private boolean isVisible;
}
