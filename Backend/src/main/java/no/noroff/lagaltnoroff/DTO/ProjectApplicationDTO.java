package no.noroff.lagaltnoroff.DTO;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProjectApplicationDTO {
	private String motivation;
	private Long project;
	private Long user;
}
