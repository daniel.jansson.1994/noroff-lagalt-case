package no.noroff.lagaltnoroff.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.lagaltnoroff.model.ProjectStatus;
import no.noroff.lagaltnoroff.repositories.ProjectStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("api/v1/projectStatuses")
public class ProjectStatusController {
	@Autowired
	private ProjectStatusRepository projectStatusRepository;

	// Gets all project statuses
	@Operation(summary = "Will get all project statuses")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully got all project statuses.")
	})
	@GetMapping
	public ResponseEntity<List<ProjectStatus>> getAllProjectStatuses() {
		return new ResponseEntity<>(projectStatusRepository.findAll(), HttpStatus.OK);
	}

	// We do not have more CRUD on project statuses because it should not be deleted or added
}
