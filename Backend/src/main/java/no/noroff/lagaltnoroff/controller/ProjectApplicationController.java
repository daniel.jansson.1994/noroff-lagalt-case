package no.noroff.lagaltnoroff.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.lagaltnoroff.DTO.ProjectApplicationDTO;
import no.noroff.lagaltnoroff.model.ProjectApplication;
import no.noroff.lagaltnoroff.service.ProjectApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("api/v1/projectApplications")
public class ProjectApplicationController {
	@Autowired
	private ProjectApplicationService projectApplicationService;

	// Get an application by its id
	@Operation(summary = "Getting all project applications")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully got all the project applications"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@GetMapping("{id}")
	public ResponseEntity<List<ProjectApplication>> getAllApplicationsByProjectId(@PathVariable long id) {
		return new ResponseEntity<>(projectApplicationService.findAllApplicationsByProjectId(id), HttpStatus.OK);
	}

	// Creates a new application
	@Operation(summary = "Creates a new project application")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "Successfully created a new project application"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@PostMapping()
	public ResponseEntity<ProjectApplication> createNewProjectApplication(@RequestBody ProjectApplicationDTO projectApplicationDTO) {
		return new ResponseEntity<>(projectApplicationService.addNewApplication(projectApplicationDTO), HttpStatus.CREATED);
	}

	// "Deletes" the application and sets the isDeleted boolean to true
	@Operation(summary = "Deletes a project application by its id")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "204", description = "Successfully deleted a project application by its id"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@PutMapping("status/{id}")
	public ResponseEntity<ProjectApplication> deleteProjectApplicationById(@PathVariable long id) {
		return new ResponseEntity<>(projectApplicationService.deleteProjectApplicationById(id), HttpStatus.NO_CONTENT);
	}

	// Accepts the project application and adds the project to the user
	@PutMapping("accept/{id}")
	@Operation(summary = "Accepts the project application and adds the user to the project")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully added the project to the user"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	public ResponseEntity<ProjectApplication> acceptProjectApplicationById(@PathVariable long id) {
		return new ResponseEntity<>(projectApplicationService.acceptProjectApplicationById(id), HttpStatus.OK);
	}

	// Finds all the project applications the user has by its google key
	@Operation(summary = "Getting all project applications the user has by its google key")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully got all the project applications the user has by its google key"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@GetMapping("userGoogleKey/{key}")
	public ResponseEntity<List<ProjectApplication>> findAllApplicationsByUserGoogleKey(@PathVariable String key) {
		return new ResponseEntity<>(projectApplicationService.findAllApplicationsByUserGoogleKey(key), HttpStatus.OK);
	}
}
