package no.noroff.lagaltnoroff.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.lagaltnoroff.DTO.ProjectAllDTO;
import no.noroff.lagaltnoroff.DTO.ProjectCreationDTO;
import no.noroff.lagaltnoroff.DTO.ProjectDTO;
import no.noroff.lagaltnoroff.DTO.ProjectFetchDTO;
import no.noroff.lagaltnoroff.mapper.ProjectMapper;
import no.noroff.lagaltnoroff.model.Message;
import no.noroff.lagaltnoroff.model.Project;
import no.noroff.lagaltnoroff.repositories.ProjectRepository;
import no.noroff.lagaltnoroff.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Set;

@CrossOrigin
@RestController
@RequestMapping("api/v1/projects")
public class ProjectController {
	@Autowired
	private ProjectRepository projectRepository;
	@Autowired
	private ProjectService projectService;
	@Autowired
	private ProjectMapper projectMapper;

	// Gets all the projects
	@Operation(summary = "Getting all projects with an algorithm based on skills selected.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Got all the projects successfully")
	})
	@GetMapping("algo/{googleKey}")
	public ResponseEntity<List<ProjectAllDTO>> getAllProjectsAlgorithm(@PathVariable @Nullable String googleKey) {
		if (!googleKey.isEmpty()) {
			return new ResponseEntity<>(projectService.getAllProjectsSortedBySkills(googleKey), HttpStatus.OK);
		} else {
			List<ProjectAllDTO> projects = projectMapper.projectToProjectAllDto(projectRepository.findAll());
			return new ResponseEntity<>(projects, HttpStatus.OK);
		}
	}

	// Gets a project by the given id in path. Returns projectDTO
	@Operation(summary = "Get a project based on project id, gives back the DTO for a propject.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Got all the users correctly"),
			@ApiResponse(responseCode = "401", description = "User have to login to view the projects", content = @Content)
	})
	@GetMapping("DTO/{id}")
	public ResponseEntity<ProjectDTO> getProjectDTOById(@PathVariable long id) {
		Project project = projectRepository.findById(id).get();
		return new ResponseEntity<>(projectMapper.projectToProjectDto(project), HttpStatus.OK);
	}

	//get projects based on category
	@Operation(summary = "Get all projects based on a selected category")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Got all the category based projects")
	})
	@GetMapping("projectType={projecttype}")
	public ResponseEntity<List<Project>> getAllProjectsByCategory(@PathVariable(value = "projecttype") String projecttype) {
		return new ResponseEntity<>(projectService.getProjectsByCategory(projecttype), HttpStatus.OK);
	}

	//get projects with the name containing a search string filtered based on category
	@Operation(summary = "Get all projects based on category and filtered by certain characters")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Got all the projects with the values asked for.")
	})
	@GetMapping("search={stringsearch}/category={projecttype}")
	public ResponseEntity<List<Project>> getProjectsWithNameLikeByCategory(@PathVariable String stringsearch, @PathVariable String projecttype) {
		return new ResponseEntity<>(projectService.getProjectsWithNameByCategory(stringsearch, projecttype), HttpStatus.OK);
	}

	//get projects with name like:
	@Operation(summary = "Getting all projects with a certain set of characters in its name.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Got all the projects with name like the set of characters.")
	})
	@GetMapping("search={stringsearch}")
	public ResponseEntity<List<Project>> getAllProjectsWithNameLike(@PathVariable String stringsearch) {
		return new ResponseEntity<>(projectService.getProjectsWithNameLike(stringsearch), HttpStatus.OK);
	}

	//Update a projects values with the ID of the project
	@Operation(summary = "Updating a projects values with the ID of the project")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "The projects got successfully updated.")
	})
	@PutMapping("{id}")
	public ResponseEntity<Project> updateProjectById(@RequestBody ProjectFetchDTO projectFetchDTO, @PathVariable long id) {
		return new ResponseEntity<>(projectService.changeProjectById(projectFetchDTO, id), HttpStatus.OK);
	}

	//get 1 project by id
	@Operation(summary = "Get one projects based on the projects ID")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Got the projects successfully.")
	})
	@GetMapping("{id}")
	public ResponseEntity<Project> getProjectById(@PathVariable long id) {
		return new ResponseEntity<>(projectRepository.findById(id).get(), HttpStatus.OK);
	}

	//Get projects based by a users google key.
	@Operation(summary = "Get all projects based on a users google key")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Got all projects based on the user and its google key.")
	})
	@GetMapping("user/{key}")
	public ResponseEntity<List<Project>> getProjectsByUserGoogleKey(@PathVariable String key) {
		return new ResponseEntity<>(projectService.getByUserGoogleKey(key), HttpStatus.OK);
	}

	//Change a projects status based on id
	@Operation(summary = "change a projects status based on the project id.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Changed the status of the projects successfully.")
	})
	@PutMapping("projectstatus/{id}")
	public ResponseEntity<Project> setNewStatusForProjectById(@PathVariable long id, @RequestBody long statusId) {
		return new ResponseEntity<>(projectService.setNewProjectStatusWithId(id, statusId), HttpStatus.NO_CONTENT);
	}

	//Set skills required for a project
	@Operation(summary = "Set skills required for a project based on the project ID.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully set a new set of skills for the project.")
	})
	@PutMapping("skills/{id}")
	public ResponseEntity<Project> setNewListOfSkillsForProjectById(@PathVariable long id, @RequestBody Set<Long> skillId) {
		return new ResponseEntity<>(projectService.addSkillsForProjectWithId(id, skillId), HttpStatus.CREATED);
	}

	//Set new description for a project based on project Id
	@Operation(summary = "Giving a project a new description based on project id.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully updated the projects description.")
	})
	@PutMapping("description/{id}")
	public ResponseEntity<Project> setNewDescriptionForProjectById(@PathVariable long id, @RequestBody String projectDescription) {
		return new ResponseEntity<>(projectService.newDescriptionForProjectWithId(id, projectDescription), HttpStatus.CREATED);
	}

	//Set new title based on a projects ID.
	@Operation(summary = "Giving a project a new title based on the projects ID")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully updated the projects title based on its ID.")
	})
	@PutMapping("title/{id}")
	public ResponseEntity<Project> setNewTitleForProjectById(@PathVariable long id, @RequestBody String projectTitle) {
		return new ResponseEntity<>(projectService.newTitleForProjectWithId(id, projectTitle), HttpStatus.CREATED);
	}

	//Create a new project
	@Operation(summary = "Will create a new project and send it to the database.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "The project got successfully created.")
	})
	@PostMapping()
	public ResponseEntity<Project> createNewProject(@RequestBody ProjectCreationDTO projectCreationDTO) {
		return new ResponseEntity<>(projectService.newProjectCreator(projectCreationDTO), HttpStatus.CREATED);
	}

	//Delete a project based on project id.
	@Operation(summary = "Will change the projects deleted status to true")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "The projects status got successfully set to deleted.")
	})
	@PutMapping("status/{id}")
	public ResponseEntity<Project> deleteProjectById(@PathVariable long id) {
		return new ResponseEntity<>(projectService.deleteProjectById(id), HttpStatus.NO_CONTENT);
	}

	@Operation(summary = "Getting all the messages in a project")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully got all the messages in a project")
	})
	@GetMapping("{id}/messages")
	public ResponseEntity<Set<Message>> getAllMessagesToAProject(@PathVariable long id) {
		return new ResponseEntity<>(projectService.getById(id).getMessages(), HttpStatus.OK);
	}
}
