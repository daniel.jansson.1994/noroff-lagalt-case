package no.noroff.lagaltnoroff.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.lagaltnoroff.model.UserHistory;
import no.noroff.lagaltnoroff.service.UserHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("api/v1/userHistories")
public class UserHistoryController {
	@Autowired
	private UserHistoryService userHistoryService;

	// Getting all the user histories
	@Operation(summary = "Getting all the user histories")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "Successfully got all the user histories"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@GetMapping
	public ResponseEntity<List<UserHistory>> getAllUserHistories() {
		return new ResponseEntity<>(userHistoryService.findAll(), HttpStatus.OK);
	}

	// Getting user history by id
	@Operation(summary = "Getting a user history by its id")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully got the user history by its id"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@GetMapping("{id}")
	public ResponseEntity<UserHistory> getUserHistoryById(@PathVariable long id) {
		return new ResponseEntity<>(userHistoryService.getById(id), HttpStatus.OK);
	}

	// Adding a new user history
	@Operation(summary = "Creating a new user history")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "Successfully got all the user histories for the user"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@PostMapping("add")
	public ResponseEntity<UserHistory> createNewUserHistory(@RequestBody UserHistory userHistory) {
		return new ResponseEntity<>(userHistoryService.save(userHistory), HttpStatus.CREATED);
	}

	// Updating a user history by its id
	@Operation(summary = "Updates a user history by its id")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully updated a user history"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@PutMapping("{id}")
	public ResponseEntity<Boolean> updateUserHistoryById(@RequestBody UserHistory userHistory, @PathVariable long id) {
		return new ResponseEntity<>(userHistoryService.updateUserHistoryById(userHistory, id), HttpStatus.CREATED);
	}

	// Deletes the user history by the given id in the path
	@Operation(summary = "Deletes a user history by its id")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "204", description = "Successfully deleted the user history by its id"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@PutMapping("delete/{id}")
	public ResponseEntity<UserHistory> deleteUserHistoryById(@PathVariable long id) {
		return new ResponseEntity<>(userHistoryService.deleteUserHistoryById(id), HttpStatus.NO_CONTENT);
	}
}
