package no.noroff.lagaltnoroff.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.lagaltnoroff.model.Skill;
import no.noroff.lagaltnoroff.service.SkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("api/v1/skills")
public class SkillController {
	@Autowired
	private SkillService skillService;

	// Gets all the skills
	@Operation(summary = "Getting all skills")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully got all skills"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@GetMapping
	public ResponseEntity<List<Skill>> getAllSkills() {
		return new ResponseEntity<>(skillService.findAll(), HttpStatus.OK);
	}

	// Get the skill by the given id in path
	@Operation(summary = "Getting a skill by its id")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully got the skill by its id"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@GetMapping("{id}")
	public ResponseEntity<Skill> getSkillById(@PathVariable long id) {
		return new ResponseEntity<>(skillService.getById(id), HttpStatus.OK);
	}

	// Adds a new skill
	@Operation(summary = "Creating a new skill")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "Successfully created a new skill"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@PostMapping()
	public ResponseEntity<Skill> createNewSkill(@RequestBody Skill skill) {
		return new ResponseEntity<>(skillService.save(skill), HttpStatus.CREATED);
	}

	// Updating a skill by given id in path
	@Operation(summary = "Updates a skill by its id")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully updated a skill by its id"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@PutMapping("{id}")
	public ResponseEntity<Boolean> updateExistingSkillById(@RequestBody Skill skill, @PathVariable long id) {
		return new ResponseEntity<>(skillService.updateSkillById(skill, id), HttpStatus.CREATED);
	}

	// Deletes a skill by given id in path
	@Operation(summary = "Deletes a skill by its id")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully deleted a skill by its id"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@DeleteMapping("{id}")
	public ResponseEntity<Boolean> deleteSkillById(@PathVariable long id) {
		return new ResponseEntity<>(skillService.deleteSkillById(id), HttpStatus.NO_CONTENT);
	}
}
