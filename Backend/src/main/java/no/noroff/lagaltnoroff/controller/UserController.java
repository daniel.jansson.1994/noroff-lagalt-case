package no.noroff.lagaltnoroff.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import no.noroff.lagaltnoroff.DTO.UserCreationDTO;
import no.noroff.lagaltnoroff.DTO.UserDTO;
import no.noroff.lagaltnoroff.DTO.UserSmallDTO;
import no.noroff.lagaltnoroff.DTO.UserUpdateDTO;
import no.noroff.lagaltnoroff.mapper.UserMapper;
import no.noroff.lagaltnoroff.model.Message;
import no.noroff.lagaltnoroff.model.User;
import no.noroff.lagaltnoroff.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@CrossOrigin
@RestController
@RequestMapping("api/v1/users")
public class UserController {
	@Autowired
	private UserService userService;
	@Autowired
	private UserMapper userMapper;

	// Gets all the users
	@Operation(summary = "Getting all the users", security = @SecurityRequirement(name = "Bearer token"))
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Got all the users correctly"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@GetMapping
	public ResponseEntity<List<UserDTO>> getAllUsers() {
		return new ResponseEntity<>(userService.findAll(), HttpStatus.OK);
	}

	/**
	 *Creates a new user with userCreationDTO which has name, description,
	 * accomplishments and a boolean value to the user being visible
	 */
	@Operation(summary = "Creating a new user")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "Created a new user correctly"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@PostMapping()
	public ResponseEntity<User> createNewUser(@RequestBody UserCreationDTO userCreationDTO) {
		return new ResponseEntity<>(userService.save(userCreationDTO), HttpStatus.CREATED);
	}

	//Gets the user by google key, returns userDTO
	@Operation(summary = "Getting a specific user by its google key")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Got the user correctly"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@GetMapping("{googleKey}")
	public ResponseEntity<UserDTO> getUserByGoogleKey(@PathVariable String googleKey) {
		User user = userService.getUserByGoogleKey(googleKey);
		return new ResponseEntity<>(userMapper.userToUserDTO(user), HttpStatus.OK);
	}

	// Returns a boolean if the google key exists
	@Operation(summary = "Returns a boolean if the google key exists by a user")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully checked if a user is exists by google key"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@GetMapping("{googleKey}/exists")
	public ResponseEntity<Boolean> userExistsByGoogleKey(@PathVariable String googleKey) {
		return new ResponseEntity<>(userService.userExistsByGoogleKey(googleKey), HttpStatus.OK);
	}

	// Gets the projects the user is in
	@Operation(summary = "Getting all the projects a user has")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully got all the projects the user has"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@GetMapping("{googleKey}/projects")
	public ResponseEntity<Set<String>> getAllProjectsByUserGoogleKey(@PathVariable String googleKey) {
		return new ResponseEntity<>(userService.getUserByGoogleKey(googleKey).getProjectsAsString(), HttpStatus.OK);
	}

	// Adds a new project with the given id in the request body to the user by Google key
	@Operation(summary = "Adding a new project to a user with the users google key")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "Successfully added a project for the user"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@PutMapping("{googleKey}/projects")
	public ResponseEntity<User> addProjectToUserGoogleKey(@PathVariable String googleKey, @RequestBody long projectId) {
		return new ResponseEntity<>(userService.addProjectByUserGoogleKey(googleKey, projectId), HttpStatus.CREATED);
	}

	// Removes the project by id given in the request body to the user with given Google key in the path
	@Operation(summary = "Remove a new project to a user with the users google key. This takes an integer in the request body")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "Successfully removed a project for the user"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@PutMapping("{googleKey}/projects/delete")
	public ResponseEntity<User> deleteProjectFromUserGoogleKey(@PathVariable String googleKey, @RequestBody long projectId) {
		return new ResponseEntity<>(userService.removeProjectByUserGoogleKey(googleKey, projectId), HttpStatus.OK);
	}

	// Gets all skills the user has selected by Google key
	@Operation(summary = "Getting all the skills a user has by its google key")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully got all the skills for a user"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@GetMapping("{googleKey}/skills")
	public ResponseEntity<Set<String>> getSkillsByUserGoogleKey(@PathVariable String googleKey) {
		return new ResponseEntity<>(userService.getUserByGoogleKey(googleKey).getSkillsAsString(), HttpStatus.OK);
	}

	// Adding skills from a set of ints in the request body to the user by google key
	@Operation(summary = "Adding new skills to a user with the users google key. Can take an array of integers")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "Successfully added a project for the user"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@PutMapping("{googleKey}/skills")
	public ResponseEntity<User> addSkillsToUserWithGoogleKey(@PathVariable String googleKey, @RequestBody Set<Long> skillIds) {
		return new ResponseEntity<>(userService.addSkillsForUserWithGoogleKey(googleKey, skillIds), HttpStatus.CREATED);
	}

	/*
	Removing skills the user has from a set of ints in the request body from the user by google key
	 */
	@Operation(summary = "Removing new skills to a user with the users google key. Can take an array of integers")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully removed a project for the user"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@PutMapping("{googleKey}/skills/delete")
	public ResponseEntity<User> deleteSkillsToUserWithGoogleKey(@PathVariable String googleKey, @RequestBody Set<Long> skillIds) {
		return new ResponseEntity<>(userService.deleteSkillsForUserWithGoogleKey(googleKey, skillIds), HttpStatus.OK);
	}

	/*
	 Updates a user by google key. Using UserUpdateDTO to get the new values the user should have.
	 UserUpdateDTO can take:
	 A new name, new description, new accomplishments, set of ints for project ids, set of ints for skill ids.
	 */
	@Operation(summary = "Updates the user by the given google key.")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "Successfully updated the user"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@PutMapping("{googleKey}")
	public ResponseEntity<User> updateUserByGoogleKey(@RequestBody UserUpdateDTO userUpdateDTO, @PathVariable String googleKey) {
		return new ResponseEntity<>(userService.updateUserByGoogleKey(userUpdateDTO, googleKey), HttpStatus.CREATED);
	}

	// Basically deletes the user, setting the boolean isDeleted to true to the user by google key.
	@Operation(summary = "Sets the user boolean isDeleted to true")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully changed the isDeleted boolean to true"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@PutMapping("status/{googleKey}")
	public ResponseEntity<User> deleteUserByGoogleKey(@PathVariable String googleKey) {
		return new ResponseEntity<>(userService.changeUserStatusById(googleKey), HttpStatus.OK);
	}

	// Gets all the messages the user has sent by google key.
	@Operation(summary = "Getting all the messages the user has")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully got all the users messages"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@GetMapping("{googleKey}/messages")
	public ResponseEntity<Set<String>> getAllMessagesByUserGoogleKey(@PathVariable String googleKey) {
		return new ResponseEntity<>(userService.getUserByGoogleKey(googleKey).getMessagesAsString(), HttpStatus.OK);
	}

	// Adds a message to the user by the given googleKey and the project id with the message in the request body
	@Operation(summary = "Adding a new message to the user by its google key")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully added a new message to the user"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@PutMapping("{googleKey}/messages/{projectId}")
	public ResponseEntity<Message> addMessagesToUserByGoogleKey(@PathVariable(value = "googleKey") String googleKey, @PathVariable(value = "projectId") long projectId, @RequestBody String message) {
		return new ResponseEntity<>(userService.addMessageToUserWithGoogleKey(googleKey, message, projectId), HttpStatus.OK);
	}

	// Gets all the user histories the user has by google key. User histories are projects the user has visited.
	@Operation(summary = "Getting all the user histories for the user by its google key")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully got all the user histories for the user"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@GetMapping("{googleKey}/userhistories")
	public ResponseEntity<Set<String>> getAllUserHistoriesByUserGoogleKey(@PathVariable String googleKey) {
		return new ResponseEntity<>(userService.getUserByGoogleKey(googleKey).getUserHistoriesAsString(), HttpStatus.OK);
	}

	// Adds a userhistory to the user by google key.
	@Operation(summary = "Adds a new user history to the user by its google key")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully added a new user history for the user"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@PutMapping("{googleKey}/userhistories")
	public ResponseEntity<Set<String>> addUserHistoryToUserByGoogleKey(@PathVariable String googleKey, @RequestBody long uhId) {
		return new ResponseEntity<>(userService.addUserHistoryToUserWithGoogleKey(googleKey, uhId).getUserHistoriesAsString(), HttpStatus.OK);
	}

	// Adding google key to the user by user id. This is done the second the user has logged in and got a google key value
	@Operation(summary = "Adding a google key to a user by its id")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "Successfully added a google key for the user"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@PutMapping("{id}/gk")
	public ResponseEntity<User> addGoogleKeyToUserById(@PathVariable long id, @RequestBody String googleKey) {
		return new ResponseEntity<>(userService.addGoogleKeyToUserById(id, googleKey), HttpStatus.CREATED);
	}

	// Getting the user's name and google key by the user google key. This endpoint is making the work on the front end easier
	@Operation(summary = "Gets the users name and google key by its google key")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully got the user by its google key"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@GetMapping("user/{googleKey}")
	public ResponseEntity<UserSmallDTO> getUserByIdShowGoogleKeyAndName(@PathVariable String googleKey) {
		User user = userService.getUserByGoogleKey(googleKey);
		return new ResponseEntity<>(userMapper.userToUserSmallDTO(user), HttpStatus.OK);
	}
}
