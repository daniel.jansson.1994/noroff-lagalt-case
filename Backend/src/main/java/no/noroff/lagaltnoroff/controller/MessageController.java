package no.noroff.lagaltnoroff.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.lagaltnoroff.model.Message;
import no.noroff.lagaltnoroff.repositories.MessageRepository;
import no.noroff.lagaltnoroff.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("api/v1/messages")
public class MessageController {
	@Autowired
	private MessageRepository messageRepository;
	@Autowired
	private MessageService messageService;

	// Gets all the messages
	@Operation(summary = "Getting all messages")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully got all the messages"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@GetMapping
	public ResponseEntity<List<Message>> getAllMessages() {
		HttpStatus status = HttpStatus.OK;
		return new ResponseEntity<>(messageRepository.findAll(), status);
	}

	// Creating a new message
	@Operation(summary = "Creating a new message")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "Successfully created a new message"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@PostMapping("add")
	public ResponseEntity<Message> createNewMessage(@RequestBody Message message) {
		return new ResponseEntity<>(messageRepository.save(message), HttpStatus.CREATED);
	}

	// Updates a new message by the given id in path
	@Operation(summary = "Updating a message by its id")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "Successfully updated a message"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@PutMapping("{id}")
	public ResponseEntity<Boolean> updateExistingMessageById(@RequestBody Message message, @PathVariable long id) {
		return new ResponseEntity<>(messageService.updateMessageById(message, id), HttpStatus.CREATED);
	}

	// "Deletes" the message by the given id in path
	@Operation(summary = "Delete a message by its id")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "Successfully deleted a message by its id"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@PutMapping("status/{id}")
	public ResponseEntity<Message> deleteMessageById(@PathVariable long id) {
		return new ResponseEntity<>(messageService.deleteMessageById(id), HttpStatus.NO_CONTENT);
	}
}
