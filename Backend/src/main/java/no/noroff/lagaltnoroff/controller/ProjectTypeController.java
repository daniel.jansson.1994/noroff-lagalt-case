package no.noroff.lagaltnoroff.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.lagaltnoroff.model.ProjectType;
import no.noroff.lagaltnoroff.repositories.ProjectTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("api/v1/projectTypes")
public class ProjectTypeController {
	@Autowired
	private ProjectTypeRepository projectTypeRepository;

	// Gets all the project types
	@Operation(summary = "Getting all the project types")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Successfully got all project types"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@GetMapping
	public ResponseEntity<List<ProjectType>> getAllProjectTypes() {
		HttpStatus status = HttpStatus.OK;
		return new ResponseEntity<>(projectTypeRepository.findAll(), status);
	}

	// Adding a new project type
	@Operation(summary = "Creating a new project type")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "Successfully created a new project type"),
			@ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content)
	})
	@PostMapping("add")
	public ResponseEntity<ProjectType> createProjectType(@RequestBody ProjectType projectType) {
		return new ResponseEntity<>(projectTypeRepository.save(projectType), HttpStatus.CREATED);
	}

	// We do not have more CRUD on this because the project type should not he deleted or changed
}
