package no.noroff.lagaltnoroff.security;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FirebaseProperties {
	int sessionExpiresInNumberOfDays;
	String databaseUrl;
	boolean enableServerSession;
	boolean revokeSessionCheckEnabled;
	boolean enableLogoutEverywhere;
}
