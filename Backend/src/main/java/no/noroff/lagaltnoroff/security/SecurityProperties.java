package no.noroff.lagaltnoroff.security;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConfigurationProperties("security")
@Getter
@Setter
@NoArgsConstructor
public class SecurityProperties {
	FirebaseProperties firebaseProperties;
	boolean allowCredentials;
	List<String> allowedOrigins;
	List<String> allowedHeaders;
	List<String> exposedHeaders;
	List<String> allowedMethods;
	List<String> allowedPublicApis;

	public SecurityProperties(FirebaseProperties firebaseProps, boolean allowCredentials, List<String> allowedOrigins, List<String> allowedHeaders, List<String> exposedHeaders, List<String> allowedMethods, List<String> allowedPublicApis) {
		this.firebaseProperties = firebaseProps;
		this.allowCredentials = allowCredentials;
		this.allowedOrigins = allowedOrigins;
		this.allowedHeaders = allowedHeaders;
		this.exposedHeaders = exposedHeaders;
		this.allowedMethods = allowedMethods;
		this.allowedPublicApis = allowedPublicApis;
	}
}