package no.noroff.lagaltnoroff.security.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class User implements Serializable {
	//Firebase user

	//
	@Serial
	private static final long serialVersionUID = 378013894234L;
	//private static final long serialVersionUID = 4408418647685225829L;
	private String uid;
	private String name;
	private String email;
	private boolean isEmailVerified;
}
