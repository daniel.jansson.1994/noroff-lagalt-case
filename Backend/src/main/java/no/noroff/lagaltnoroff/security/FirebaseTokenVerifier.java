package no.noroff.lagaltnoroff.security;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;
import no.noroff.lagaltnoroff.security.model.Credentials;
import no.noroff.lagaltnoroff.security.model.User;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class FirebaseTokenVerifier extends OncePerRequestFilter {
	// Getting the bearer token string from Authorization and removes the "Bearer" String
	public static String getBearerToken(HttpServletRequest request) {
		String bearerToken = null;
		try {
			String authorization = request.getHeader("Authorization");
			if (authorization != null) {
				bearerToken = authorization.substring(7);
			}

		} catch (NullPointerException e) {
			e.printStackTrace();
		}
		return bearerToken;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
		response.setHeader("Access-Control-Allow-Methods", request.getHeader("GET, PUT, POST, DELETE, PATCH, OPTIONS"));
		firebaseTokenVerifier(request);
		filterChain.doFilter(request, response);
	}

	// Verifies the token from the request header. "Trims" the bearer token nad verifies it through Firebase auth
	public static void firebaseTokenVerifier(HttpServletRequest request) {
		String idToken = getBearerToken(request);
		Credentials.CredentialType type = null;
		FirebaseToken decodedToken = null;
		try {
			if (idToken != null) {
				decodedToken = FirebaseAuth.getInstance().verifyIdToken(idToken);
				type = Credentials.CredentialType.ID_TOKEN;
			} else {
				idToken = "m";
			}
		} catch (FirebaseAuthException e) {
			e.printStackTrace();
		}
		User user = firebaseTokenToFirebaseUser(decodedToken);
		if (user != null) {
			UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user,
					new Credentials(type, decodedToken, idToken), null);
			authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
			SecurityContextHolder.getContext().setAuthentication(authentication);
		}
	}

	// Creates a firebase user from the decoded firebase token
	private static User firebaseTokenToFirebaseUser(FirebaseToken decodedToken) {
		User user = null;
		if (decodedToken != null) {
			user = new User();
			user.setUid(decodedToken.getUid());
			user.setName(decodedToken.getName());
			user.setEmail(decodedToken.getEmail());
			user.setEmailVerified(decodedToken.isEmailVerified());
		}
		return user;
	}
}
