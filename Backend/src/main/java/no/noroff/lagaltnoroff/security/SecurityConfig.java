package no.noroff.lagaltnoroff.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, jsr250Enabled = true, prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	private ObjectMapper objectMapper;
	@Autowired
	public FirebaseTokenVerifier firebaseTokenVerifier;
	@Autowired
	private SecurityProperties secProps;


	// Sets the response code
	@Bean
	public AuthenticationEntryPoint restAuthenticationEntryPoint() {
		return (httpServletRequest, httpServletResponse, e) -> {
			Map<String, Object> errorMessage = new HashMap<>();
			int errorCode = 401;
			errorMessage.put("message", "You do not have a valid token, please log in to receive a token and try again.");
			errorMessage.put("error", HttpStatus.UNAUTHORIZED);
			errorMessage.put("code", errorCode);
			errorMessage.put("timestamp", new Timestamp(new Date().getTime()));
			httpServletResponse.setContentType("application/json;charset=UTF-8");
			httpServletResponse.setStatus(errorCode);
			httpServletResponse.getWriter().write(objectMapper.writeValueAsString(errorMessage));
		};
	}

	@Bean
	public CorsConfigurationSource corsConfigurationSource() {
		CorsConfiguration config = new CorsConfiguration();
		config.addAllowedHeader("*");
		config.addAllowedMethod("*");
		config.addAllowedOrigin("https://noroff-lagalt-web-app.herokuapp.com");
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", config);
		return source;
	}

	// Security enable which endpoints are allowed to the public and which endpoints the user needs to be verified
	/*@Override
	public void configure(WebSecurity web) throws Exception{
		web.ignoring().antMatchers("api/v1/**");
	}*/

	// Configure the cors settings and authorizations
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.cors().configurationSource(corsConfigurationSource()).and().csrf().disable().formLogin().disable()
				.httpBasic().disable().exceptionHandling().authenticationEntryPoint(restAuthenticationEntryPoint())
				.and().authorizeRequests()
				.antMatchers(secProps.getAllowedPublicApis().toArray(String[]::new)).permitAll().and()
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests(authorize -> {
					authorize.antMatchers("/swagger-ui/**", "/api-docs/**").permitAll()
							.antMatchers("/api/v1/projects/**").permitAll();
				});
		http.addFilterBefore(firebaseTokenVerifier, UsernamePasswordAuthenticationFilter.class);
	}
}
