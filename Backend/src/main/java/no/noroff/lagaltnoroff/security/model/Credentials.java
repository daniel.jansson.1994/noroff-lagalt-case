package no.noroff.lagaltnoroff.security.model;

import com.google.firebase.auth.FirebaseToken;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Credentials {
	private CredentialType type;
	private FirebaseToken decodedToken;
	private String idToken;

	public enum CredentialType {
		ID_TOKEN, SESSION
	}

	public Credentials(CredentialType type, FirebaseToken decodedToken, String idToken) {
		this.type = type;
		this.decodedToken = decodedToken;
		this.idToken = idToken;
	}
}
