package no.noroff.lagaltnoroff.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@NoArgsConstructor
public class Message implements Serializable {
	/*
	 * id - pk
	 * message
	 * time and date sent / edited
	 * deleted
	 * user - fk
	 * project - fk
	 */

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	private long id;
	private String message;
	private String dateSent = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss"));
	private boolean isDeleted = false;

	@ManyToOne
	private User user;

	@ManyToOne
	private Project project;

	@JsonGetter("user")
	public String getUserAsString() {
		if (user != null) {
			return user.getName();
		} else
			return null;
	}

	@JsonGetter("project")
	public String getProjectAsString() {
		if (project != null) {
			return project.getTitle();
		} else {
			return null;
		}
	}

	public Message(String message, String dateSent, User user, Project project) {
		this.message = message;
		this.dateSent = dateSent;
		this.user = user;
		this.project = project;
	}
}
