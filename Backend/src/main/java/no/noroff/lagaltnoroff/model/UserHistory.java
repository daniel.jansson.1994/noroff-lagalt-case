package no.noroff.lagaltnoroff.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Setter
@Getter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@NoArgsConstructor
public class UserHistory implements Serializable {
	/*
	 * id - pk
	 * name
	 * user - fk
	 * project - fk
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String name;
	private boolean isDeleted = false;
	@ManyToOne
	private User user;

	@ManyToOne
	private Project project;

	public UserHistory(String name, User user, Project project) {
		this.name = name;
		this.user = user;
		this.project = project;
	}

	@JsonGetter("user")
	public String getUserAsString() {
		return user.getName();
	}

	@JsonGetter("project")
	public String getProjectAsString() {
		return project.getTitle();
	}

}
