package no.noroff.lagaltnoroff.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@NoArgsConstructor
public class Skill implements Serializable {
	/*
	 * id - pk
	 * name
	 * user - fk
	 * project - fk
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@Column(name = "skill_id")
	private long id;
	@Column(length = 40)
	private String name;

	@ManyToMany(mappedBy = "skills")
	private Set<User> users;

	@ManyToMany(mappedBy = "skills")
	private Set<Project> projects;

	public Skill(String name) {
		this.name = name;
	}

	@JsonGetter("projects")
	public Set<String> getProjectsAsString() {
		if (projects != null)
			return projects.stream().map(Project::getTitle).collect(Collectors.toSet());
		else
			return null;
	}

	@JsonGetter("users")
	public Set<String> getUsersAsString() {
		if (users != null)
			return users.stream().map(User::getName).collect(Collectors.toSet());
		else
			return null;
	}
}
