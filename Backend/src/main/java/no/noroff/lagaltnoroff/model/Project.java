package no.noroff.lagaltnoroff.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Set;
import java.util.stream.Collectors;


@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
public class Project implements Serializable {

	/*
	 * id
	 * name
	 * desc
	 * skill_required
	 * owner fk
	 * chat id fk
	 * projecttype_fk
	 * */

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column(length = 50, nullable = false)
	private String title;
	@Column(length = 1250)
	private String description;
	private boolean isDeleted = false;

	@ManyToMany
	@JoinTable(
			name = "project_skill",
			joinColumns = {@JoinColumn(name = "project_id")},
			inverseJoinColumns = {@JoinColumn(name = "skill_id")}
	)
	private Set<Skill> skills;

	@JsonGetter("skills")
	public Set<String> getSkillsAsString() {
		if (skills != null)
			return skills.stream().map(Skill::getName).collect(Collectors.toSet());
		else
			return null;
	}

	@ManyToOne
	@JoinColumn(name = "Project_Type")
	private ProjectType projectType;

	@ManyToOne
	@JoinColumn(name = "projectStatus")
	private ProjectStatus projectStatus;

	@ManyToOne
	private User owner;

	@OneToMany(mappedBy = "project")
	private Set<Message> messages;

	@OneToMany(mappedBy = "project", cascade = CascadeType.ALL)
	private Set<ProjectApplication> projectApplications;

	@OneToMany(mappedBy = "project")
	private Set<UserHistory> userHistory;

	@JsonGetter("projectApplications")
	public Set<String> getProjectApplicationsById() {
		if (projectApplications != null) {
			return projectApplications.stream().map(ProjectApplication::getUserIdAsString).collect(Collectors.toSet());
		} else
			return null;
	}

	@JsonGetter("messages")
	public Set<String> getMessageAsString() {
		if (messages != null) {
			return messages.stream().map(Message::getMessage).collect(Collectors.toSet());
		} else {
			return null;
		}
	}

	@JsonGetter("projectStatus")
	private String getProjectStatusAsString() {
		if (projectStatus != null) {
			return projectStatus.getStatus();
		} else
			return null;
	}

	@JsonGetter("projectType")
	private String getProjectTypeAsString() {
		if (projectType != null)
			return projectType.getProjecttype();
		else
			return null;
	}

	@JsonGetter("userHistory")
	public Set<Long> getUserHistoriesAsId() {
		if (userHistory != null) {
			return userHistory.stream().map(UserHistory::getId).collect(Collectors.toSet());
		} else
			return null;
	}


	@ManyToMany(mappedBy = "projects")
	public Set<User> users;


    /*
    @JsonGetter("users")
    public Set<String> getUsersAsString() {
        if (users != null)
            return users.stream().map(User::getGoogleKey).collect(Collectors.toSet());
        else
            return null;
    }
     */

	@JsonGetter("users")
	public HashMap<String, String> getUsersAsString() {
		HashMap<String, String> usersMap = new HashMap<>();
		if (users != null) {
			for (User user : users) {
				usersMap.put(user.getName(), user.getGoogleKey());
			}
			return usersMap;
		} else
			return null;
	}

	@JsonGetter("owner")
	public String getOwnerByString() {
		if (owner != null)
			return owner.getName();
		else
			return null;
	}

	@JsonGetter("OwnerGoogleKey")
	public String getOwnerKey() {
		return owner.getGoogleKey();
	}

	public Project(String title, String description, Set<Skill> skills, ProjectType projectType, ProjectStatus projectStatus, User owner, Set<Message> messages, Set<ProjectApplication> projectApplications, Set<UserHistory> userHistory) {
		this.title = title;
		this.description = description;
		this.skills = skills;
		this.projectType = projectType;
		this.projectStatus = projectStatus;
		this.owner = owner;
		this.messages = messages;
		this.projectApplications = projectApplications;
		this.userHistory = userHistory;
	}

	public Project(String title, String description, Set<Skill> skills, ProjectType projectType, ProjectStatus projectStatus, User owner, Set<Message> messages, Set<ProjectApplication> projectApplications, Set<UserHistory> userHistory, Set<User> users) {
		this.title = title;
		this.description = description;
		this.skills = skills;
		this.projectType = projectType;
		this.projectStatus = projectStatus;
		this.owner = owner;
		this.messages = messages;
		this.projectApplications = projectApplications;
		this.userHistory = userHistory;
		this.users = users;
	}
}