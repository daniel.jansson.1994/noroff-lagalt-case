package no.noroff.lagaltnoroff.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
public class ProjectStatus implements Serializable {
	/*
	 * id - pk
	 * status
	 * project - fk
	 */

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column(length = 25, nullable = false)
	private String status;

	@OneToMany(mappedBy = "projectStatus")
	private Set<Project> project;

	@JsonGetter("project")
	public Set<String> getProject() {
		if (project != null) {
			return project.stream().map(Project::getTitle).collect(Collectors.toSet());
		} else return null;
	}

	public ProjectStatus(String status) {
		this.status = status;
	}
}
