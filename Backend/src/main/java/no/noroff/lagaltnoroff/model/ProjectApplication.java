package no.noroff.lagaltnoroff.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
public class ProjectApplication implements Serializable {

	/*
	 * id - pk
	 * motivation
	 * deleted
	 * project - fk
	 * user - fk
	 */

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column(length = 500)
	private String motivation;
	private boolean isDeleted = false;

	@ManyToOne
	@JoinColumn(name = "project_id")
	private Project project;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	@JsonGetter("project")
	public String getProjectAsString() {
		return project.getTitle();
	}

	@JsonGetter("user")
	public String getUserIdAsString() {
		return user.getName();
	}

	@JsonGetter("userGoogleKey")
	public String getUserKey() {
		return user.getGoogleKey();
	}

	@JsonGetter("projectProjectId")
	public long getProjectIdAsLong() {
		return project.getId();
	}

	public ProjectApplication(String motivation, Project project, User user) {
		this.motivation = motivation;
		this.project = project;
		this.user = user;
	}
}
