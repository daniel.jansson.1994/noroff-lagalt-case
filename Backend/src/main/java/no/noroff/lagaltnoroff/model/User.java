package no.noroff.lagaltnoroff.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "Users")
public class User implements Serializable {
	/*
	 * id - pk
	 * name
	 * description
	 * accomplishments
	 * visible
	 * google key
	 * is deleted
	 * skill - fk
	 * message - fk
	 * project - fk
	 * owner - project - fk
	 * user history - fk
	 * project application - fk
	 */
	@Id
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id", unique = true)
	private long id;
	@Column(length = 50, nullable = false)
	private String name;
	private String description;
	private String accomplishments;
	private boolean isVisible;
	@Column(length = 50, unique = true)
	private String googleKey;
	private boolean isDeleted = false;

	@ManyToMany
	@JoinTable(
			name = "user_skill",
			joinColumns = {@JoinColumn(name = "user_id")},
			inverseJoinColumns = {@JoinColumn(name = "skill_id")}
	)
	private Set<Skill> skills;


	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	private Set<Message> messages;

	@ManyToMany
	@JoinTable(
			name = "user_project",
			joinColumns = {@JoinColumn(name = "user_id")},
			inverseJoinColumns = {@JoinColumn(name = "project_id")}
	)
	private Set<Project> projects;

	@OneToMany(mappedBy = "owner", cascade = CascadeType.ALL)
	private Set<Project> userOwnedProjects;

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	private Set<UserHistory> userHistories;

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	private Set<ProjectApplication> projectApplications;

	@JsonGetter("projects")
	public Set<String> getProjectsAsString() {
		if (projects != null)
			return projects.stream().map(Project::getTitle).collect(Collectors.toSet());
		else
			return null;
	}

	@JsonGetter("projectApplications")
	public Set<String> getProjectApplicationsById() {
		if (projectApplications != null) {
			return projectApplications.stream().map(ProjectApplication::getProjectAsString).collect(Collectors.toSet());
		} else
			return null;
	}

	@JsonGetter("skills")
	public Set<String> getSkillsAsString() {
		if (skills != null)
			return skills.stream().map(Skill::getName).collect(Collectors.toSet());
		else
			return null;
	}

	@JsonGetter("userOwnedProjects")
	public Set<String> getUserOwnedProjectsAsString() {
		if (userOwnedProjects != null)
			return userOwnedProjects.stream().map(Project::getTitle).collect(Collectors.toSet());
		else
			return null;
	}

	@JsonGetter("userHistories")
	public Set<String> getUserHistoriesAsString() {
		if (userHistories != null)
			return userHistories.stream().map(UserHistory::getName).collect(Collectors.toSet());
		else
			return null;
	}

	@JsonGetter("messages")
	public Set<String> getMessagesAsString() {
		if (messages != null)
			return messages.stream().map(Message::getMessage).collect(Collectors.toSet());
		else
			return null;
	}

	public User(String name, String description, String accomplishments, boolean isVisible, Set<Skill> skills, Set<Message> messages, Set<Project> projects, Set<Project> userOwnedProjects, Set<UserHistory> userHistories, Set<ProjectApplication> projectApplications, String googleKey) {
		this.name = name;
		this.description = description;
		this.accomplishments = accomplishments;
		this.isVisible = isVisible;
		this.skills = skills;
		this.messages = messages;
		this.projects = projects;
		this.userOwnedProjects = userOwnedProjects;
		this.userHistories = userHistories;
		this.projectApplications = projectApplications;
		this.googleKey = googleKey;
	}
}
