package no.noroff.lagaltnoroff.model;


import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@NoArgsConstructor
public class ProjectType implements Serializable {
	/*
	 * id - pk
	 * project type
	 * project - fk
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column(length = 50)
	private String projecttype;

	@OneToMany(mappedBy = "projectType")
	private Set<Project> project;

	@JsonGetter("project")
	public Set<String> getProject() {
		if (project != null) {
			return project.stream().map(Project::getTitle).collect(Collectors.toSet());
		} else {
			return null;
		}
	}

	public ProjectType(String projecttype) {
		this.projecttype = projecttype;
	}
}
