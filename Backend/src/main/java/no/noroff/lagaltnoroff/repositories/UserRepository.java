package no.noroff.lagaltnoroff.repositories;

import no.noroff.lagaltnoroff.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	// Autogenerated to find user by google key
	User findUserByGoogleKey(String googleKey);

	// Autogenerated to see if the google key exists.
	Boolean existsByGoogleKey(String googleKey);
}
