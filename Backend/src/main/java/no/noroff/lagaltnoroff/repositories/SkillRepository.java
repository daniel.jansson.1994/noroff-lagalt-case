package no.noroff.lagaltnoroff.repositories;

import no.noroff.lagaltnoroff.model.Skill;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SkillRepository extends JpaRepository<Skill, Long> {
}
