# LAGALT
This README is for the backend for the assignment lagalt. This backend contains the creating of models in a database. Repositories, services, endpoints and authorization with Firebase.

## Cloning the application
To clone this project with cmd.
```bash
git clone https://gitlab.com/daniel.jansson.1994/noroff-lagalt-case
```

## Using the application
To run this application you have to press the run button or go to the [Main file](src/main/java/no/noroff/lagaltnoroff/LagaltNoroffApplication.java) and run it from there in your preferred IDE.

To use postgres you have to install [PgAdmin 4](https://www.pgadmin.org/download/). Change the username and password for postgres in the [application.properties](src/main/resources/application.properties) file to what you set up your PgAdmin 4 to.

If you are using IntelliJ you can configure to use postgres as your datasource and access your database locally in IntelliJ.

## Contributors
* [Elisabeth Bruce](https://gitlab.com/ethoyer)
* [Daniel Jansson](https://gitlab.com/daniel.jansson.1994)
* [Kristian Jul-Larsen](https://gitlab.com/ludakr1ss)
* [Rosy Oo](https://gitlab.com/rosyoo)
## Technologies and dependencies

* Spring Framework
  * Security
  * JPA
  * Hibernate
* Postgres
* Firebase
* Google API
* Docker
* Mapstruct
* Lombok
* Maven Central Repository

## Links
Link to our [endpoints](https://noroff-legalt-temp-db.herokuapp.com/swagger-ui/index.html#/) showed with Swagger documentation API.

Link to our [frontend](https://noroff-lagalt-web-app.herokuapp.com/) Web App hosted on Heroku

## Entity Resources Diagram
![](src/main/resources/LagaltERD.jpeg)
